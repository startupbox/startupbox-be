import { NestFactory } from '@nestjs/core';
import { AppModule } from '../src/app.module';
import { Connection } from 'typeorm';

// eslint-disable-next-line func-style
async function initSeed(): Promise<void> {
  console.log('Initializing database schema...');
  const app = await NestFactory.create(AppModule);

  console.log('Getting connection...');
  const connection = app.get<Connection>(Connection);
  console.log('Dropping connection...');
  await connection.dropDatabase();
  console.log('Synchronizing...');
  await connection.synchronize(true);
  await app.close();
  console.log('Closed app module.');
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
initSeed();
