## Setup

Prerequisites for backend to work:

1. PostgreSQL DB (<a href="https://www.postgresql.org/download/">install locally</a> or pull a <a href="https://hub.docker.com/_/postgres">Docker image</a>)
2. NodeJS LTS Version (v20)
3. <a href="https://github.com/nestjs/nest-cli"> NestJS CLI </a> (`optional`)

## Installation

1. Install all your packages

```bash
# install all the dependencies
$ yarn install
```

2. Setup your `.env.development` & `.env.test` files on the root of the project by copying the contents of `.env.example` and setting your own values (use different DBs for `development` and for `test`)

```bash
# create a new .env file & copy contents over
$ touch .env.development && touch .env.test
$ cat .env.example > .env.development
$ cat .env.example > .env.test
```

3. Generate ORM Config - this generates `ormconfig.json` on the root of the project for TypeORM

```bash
# generate ormconfig.json
$ yarn typeorm:setup
```

4. Start your database server and run migrations

```bash
# perform migrations to pg db
$ yarn db:migrate
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# unit tests sequentially
$ yarn test:sqnc

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Migrations

If any object entity is changed in the code-first approach, make sure to generate new migrations & run them afterwards

```bash
# generate new migrations - do not forget to add a migration name
$ yarn db:migration:generate NEW_MIGRATION_NAME_DESCRIBING_CHANGES

# perform migrations to pg db
$ yarn db:migrate
```
