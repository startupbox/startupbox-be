import _ from 'lodash';
import { UserEntity } from '../../src/components/auth/entity/user.entity';
import { UserRole } from '../../src/components/auth/interfaces/userRole.enum';

export const userFactory = (params?: Partial<UserEntity>): UserEntity => {
  return _.merge(
    {
      id: '336bf0ec-7379-4942-914a-c55e45c31496',
      createdAt: new Date('2020-07-14T12:21:02.490+00:00'),
      updatedAt: new Date('2020-07-14T12:21:02.490+00:00'),
      firstName: 'John',
      lastName: 'Wick',
      email: 'john.wick@email.com',
      role: UserRole.CLIENT,
      profilePicture:
        'https://subdomain.some-random-cdn.com/dms/image/C7D03AQGVgLnFF3gDHg/profile-displayphoto-shrink_200_200/0?e=1600300800&v=beta&t=8WZdW1eNCio0DyfLfvPuGe_BdJQrQ-E-bFYtkS6xxxo',
      password: null,
      isVerified: false,
      onboardingData: null,
      archived: false,
    },
    params
  ) as UserEntity;
};
