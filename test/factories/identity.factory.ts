import _ from 'lodash';
import { IdentityEntity } from '../../src/components/auth/entity/identity.entity';
import { AuthProvider } from '../../src/components/auth/interfaces/authProvider.enum';

export const identityFactory = (params?: Partial<IdentityEntity>): IdentityEntity => {
  return _.merge(
    {
      id: 'f339f2f8-d2cf-40fa-bb8c-a9dcc42002e8',
      createdAt: new Date('2020-07-14T12:21:02.490+00:00'),
      updatedAt: new Date('2020-07-14T12:21:02.490+00:00'),
      userId: '336bf0ec-7379-4942-914a-c55e45c31496',
      provider: AuthProvider.LINKEDIN,
    },
    params
  ) as IdentityEntity;
};
