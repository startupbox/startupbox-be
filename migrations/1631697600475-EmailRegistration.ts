import {MigrationInterface, QueryRunner} from "typeorm";

export class EmailRegistration1631697600475 implements MigrationInterface {
    name = 'EmailRegistration1631697600475'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "refresh_token_entity" ("id" character varying NOT NULL, "revoked" boolean NOT NULL DEFAULT false, "provider" character varying, "userId" uuid, CONSTRAINT "PK_a78813e06745b2c5d5b9776bfcf" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "registration_token_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "verificationToken" character varying NOT NULL, "password" character varying NOT NULL, "email" character varying NOT NULL, "used" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_baeb96fa441558fa65b5b05e90a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "isVerified" boolean`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "password" character varying`);
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "firstName" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "lastName" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" ADD CONSTRAINT "FK_ebf65cd067163c7c66baa3da1c1" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" DROP CONSTRAINT "FK_ebf65cd067163c7c66baa3da1c1"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "lastName" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "firstName" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "password"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "isVerified"`);
        await queryRunner.query(`DROP TABLE "registration_token_entity"`);
        await queryRunner.query(`DROP TABLE "refresh_token_entity"`);
    }

}
