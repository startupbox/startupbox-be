import {MigrationInterface, QueryRunner} from "typeorm";

export class OrderedPhasesColumn1595579297702 implements MigrationInterface {
    name = 'OrderedPhasesColumn1595579297702'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "phase_entity" ADD "order" integer NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "phase_entity" DROP COLUMN "order"`);
    }

}
