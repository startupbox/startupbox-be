import {MigrationInterface, QueryRunner} from "typeorm";

export class EnumeratingTasks1596281513339 implements MigrationInterface {
    name = 'EnumeratingTasks1596281513339'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" DROP COLUMN "howToPage"`);
        await queryRunner.query(`ALTER TABLE "component_entity" ADD "order" integer NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "component_entity" DROP COLUMN "order"`);
        await queryRunner.query(`ALTER TABLE "task_entity" ADD "howToPage" character varying NOT NULL`);
    }

}
