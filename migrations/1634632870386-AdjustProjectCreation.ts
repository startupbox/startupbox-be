import {MigrationInterface, QueryRunner} from "typeorm";

export class AdjustProjectCreation1634632870386 implements MigrationInterface {
    name = 'AdjustProjectCreation1634632870386'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "targetAudience"`);
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "businessType"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "projectLogoUrl" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "projectLogoUrl"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "businessType" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "targetAudience" text NOT NULL`);
    }

}
