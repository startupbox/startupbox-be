import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedIdentityEntity1594384474877 implements MigrationInterface {
    name = 'AddedIdentityEntity1594384474877'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "identity_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "userId" uuid NOT NULL, "provider" character varying NOT NULL, CONSTRAINT "PK_026974e12573ee511580aa2a46a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD CONSTRAINT "UQ_415c35b9b3b6fe45a3b065030f5" UNIQUE ("email")`);
        await queryRunner.query(`ALTER TABLE "identity_entity" ADD CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "identity_entity" DROP CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP CONSTRAINT "UQ_415c35b9b3b6fe45a3b065030f5"`);
        await queryRunner.query(`DROP TABLE "identity_entity"`);
    }

}
