import {MigrationInterface, QueryRunner} from "typeorm";

export class NullableProfilePicture1607417986593 implements MigrationInterface {
    name = 'NullableProfilePicture1607417986593'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "profilePicture" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "profilePicture" SET NOT NULL`);
    }

}
