import {MigrationInterface, QueryRunner} from "typeorm";

export class EnumeratingDiaryRelatedEntities1595403311905 implements MigrationInterface {
    name = 'EnumeratingDiaryRelatedEntities1595403311905'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "diary_entity" ADD "diaryPhase" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "diary_entity" DROP COLUMN "diaryPhase"`);
    }

}
