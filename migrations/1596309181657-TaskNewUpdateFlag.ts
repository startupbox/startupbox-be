import {MigrationInterface, QueryRunner} from "typeorm";

export class TaskNewUpdateFlag1596309181657 implements MigrationInterface {
    name = 'TaskNewUpdateFlag1596309181657'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" ADD "hasNewUpdate" boolean NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" DROP COLUMN "hasNewUpdate"`);
    }

}
