import { MigrationInterface, QueryRunner } from "typeorm";

export class RemoveLastActivityFromUserAndAddActivityLog1709728088484 implements MigrationInterface {
    name = 'RemoveLastActivityFromUserAndAddActivityLog1709728088484'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "activity_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_ae8895d1732201c8184c61a24e8" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_560b68cd00b521eaa461d2f03e" ON "activity_entity" ("userId") `);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "lastActivity"`);
        await queryRunner.query(`ALTER TABLE "activity_entity" ADD CONSTRAINT "FK_560b68cd00b521eaa461d2f03e6" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "activity_entity" DROP CONSTRAINT "FK_560b68cd00b521eaa461d2f03e6"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "lastActivity" TIMESTAMP WITH TIME ZONE`);
        await queryRunner.query(`DROP INDEX "public"."IDX_560b68cd00b521eaa461d2f03e"`);
        await queryRunner.query(`DROP TABLE "activity_entity"`);
    }

}
