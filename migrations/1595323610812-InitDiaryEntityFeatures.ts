import {MigrationInterface, QueryRunner} from "typeorm";

export class InitDiaryEntityFeatures1595323610812 implements MigrationInterface {
    name = 'InitDiaryEntityFeatures1595323610812'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "comment_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "text" character varying NOT NULL, "userId" uuid, "taskId" uuid, CONSTRAINT "PK_5a439a16c76d63e046765cdb84f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "task_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "title" character varying NOT NULL, "links" text, "completed" boolean NOT NULL, "howToPage" character varying NOT NULL, "componentId" uuid, CONSTRAINT "PK_0385ca690d1697cdf7ff1ed3c2f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "component_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "title" character varying NOT NULL, "phaseId" uuid, CONSTRAINT "PK_41c00c56b56e04026bdf3fbbb8f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "phase_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "title" character varying NOT NULL, "unlocked" boolean NOT NULL, "diaryId" uuid, CONSTRAINT "PK_c36632b58bbb4142bf2b844a87a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "diary_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "version" character varying NOT NULL, "status" character varying NOT NULL, "projectId" uuid, CONSTRAINT "REL_c77dd5dab6e06736035b4c61ba" UNIQUE ("projectId"), CONSTRAINT "PK_98ae740500de7e5d98e989e22a1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "comment_entity" ADD CONSTRAINT "FK_e391949c5735c084dddcb6e6468" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment_entity" ADD CONSTRAINT "FK_b13d3c2d34fe4eba95e757c68c3" FOREIGN KEY ("taskId") REFERENCES "task_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "task_entity" ADD CONSTRAINT "FK_667945c8db91ea681c4d39017ef" FOREIGN KEY ("componentId") REFERENCES "component_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "component_entity" ADD CONSTRAINT "FK_74fffe9ddd8123feb744a6a0144" FOREIGN KEY ("phaseId") REFERENCES "phase_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "phase_entity" ADD CONSTRAINT "FK_8eee66175be612e868afec276c4" FOREIGN KEY ("diaryId") REFERENCES "diary_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "diary_entity" ADD CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4" FOREIGN KEY ("projectId") REFERENCES "project_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "diary_entity" DROP CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4"`);
        await queryRunner.query(`ALTER TABLE "phase_entity" DROP CONSTRAINT "FK_8eee66175be612e868afec276c4"`);
        await queryRunner.query(`ALTER TABLE "component_entity" DROP CONSTRAINT "FK_74fffe9ddd8123feb744a6a0144"`);
        await queryRunner.query(`ALTER TABLE "task_entity" DROP CONSTRAINT "FK_667945c8db91ea681c4d39017ef"`);
        await queryRunner.query(`ALTER TABLE "comment_entity" DROP CONSTRAINT "FK_b13d3c2d34fe4eba95e757c68c3"`);
        await queryRunner.query(`ALTER TABLE "comment_entity" DROP CONSTRAINT "FK_e391949c5735c084dddcb6e6468"`);
        await queryRunner.query(`DROP TABLE "diary_entity"`);
        await queryRunner.query(`DROP TABLE "phase_entity"`);
        await queryRunner.query(`DROP TABLE "component_entity"`);
        await queryRunner.query(`DROP TABLE "task_entity"`);
        await queryRunner.query(`DROP TABLE "comment_entity"`);
    }

}
