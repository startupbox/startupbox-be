import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdatingSkillsStringArray1594888438682 implements MigrationInterface {
    name = 'UpdatingSkillsStringArray1594888438682'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP COLUMN "skills"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD "skills" text`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP COLUMN "skills"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD "skills" character varying NOT NULL`);
    }

}
