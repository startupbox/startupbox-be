import { MigrationInterface, QueryRunner } from "typeorm";

export class AddLastActivityToUser1709553875111 implements MigrationInterface {
    name = 'AddLastActivityToUser1709553875111'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "lastActivity" TIMESTAMP WITH TIME ZONE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "lastActivity"`);
    }

}
