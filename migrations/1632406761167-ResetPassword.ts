import {MigrationInterface, QueryRunner} from "typeorm";

export class ResetPassword1632406761167 implements MigrationInterface {
    name = 'ResetPassword1632406761167'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "reset_password_token_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "code" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "validUntil" TIMESTAMP, "userId" uuid, CONSTRAINT "PK_7c978ad4ece98a429c924906c07" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "reset_password_token_entity" ADD CONSTRAINT "FK_e487f2b7b135d9682cee3db5f16" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reset_password_token_entity" DROP CONSTRAINT "FK_e487f2b7b135d9682cee3db5f16"`);
        await queryRunner.query(`DROP TABLE "reset_password_token_entity"`);
    }

}
