import {MigrationInterface, QueryRunner} from "typeorm";

export class InitProfileEntity1594650082433 implements MigrationInterface {
    name = 'InitProfileEntity1594650082433'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "profile_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "experience" character varying NOT NULL, "region" character varying NOT NULL, "skills" character varying NOT NULL, CONSTRAINT "PK_330d3560db0dac16f06a04609bb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "profileId" uuid`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD CONSTRAINT "UQ_861abf5759299b717c5e2780a78" UNIQUE ("profileId")`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD CONSTRAINT "FK_861abf5759299b717c5e2780a78" FOREIGN KEY ("profileId") REFERENCES "profile_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP CONSTRAINT "FK_861abf5759299b717c5e2780a78"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP CONSTRAINT "UQ_861abf5759299b717c5e2780a78"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "profileId"`);
        await queryRunner.query(`DROP TABLE "profile_entity"`);
    }

}
