import {MigrationInterface, QueryRunner} from "typeorm";

export class DiaryProjectPhase1638870090909 implements MigrationInterface {
    name = 'DiaryProjectPhase1638870090909'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "phase"`);
        await queryRunner.query(`ALTER TABLE "diary_entity" ADD "projectPhase" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "diary_entity" DROP COLUMN "projectPhase"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "phase" text NOT NULL`);
    }

}
