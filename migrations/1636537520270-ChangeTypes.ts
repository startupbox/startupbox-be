import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeTypes1636537520270 implements MigrationInterface {
  name = 'ChangeTypes1636537520270';

  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "diary_entity" DROP COLUMN "diaryPhase"`);
    await queryRunner.query(`ALTER TABLE "diary_entity" ALTER COLUMN "status" TYPE text`);
    await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "phase" TYPE text`);
    await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" TYPE text`);
    await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "role" TYPE text`);
    await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "emailVerification" TYPE text`);
    await queryRunner.query(`ALTER TABLE "identity_entity" ALTER COLUMN "provider" TYPE text`);
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "identity_entity" ALTER COLUMN "provider" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "emailVerification" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "user_entity" ALTER COLUMN "role" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "phase" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "diary_entity" ALTER COLUMN "status" TYPE character varying`);
    await queryRunner.query(`ALTER TABLE "diary_entity" ADD "diaryPhase" character varying`);
  }
}
