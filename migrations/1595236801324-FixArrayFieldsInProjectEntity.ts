import {MigrationInterface, QueryRunner} from "typeorm";

export class FixArrayFieldsInProjectEntity1595236801324 implements MigrationInterface {
    name = 'FixArrayFieldsInProjectEntity1595236801324'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "targetAudience"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "targetAudience" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "industry"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "industry" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "businessType"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "businessType" text NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "businessType"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "businessType" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "industry"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "industry" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "targetAudience"`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "targetAudience" character varying NOT NULL`);
    }

}
