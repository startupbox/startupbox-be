import {MigrationInterface, QueryRunner} from "typeorm";

export class TermsAndConditions1632387739773 implements MigrationInterface {
    name = 'TermsAndConditions1632387739773'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "agreedToTermsAndConditions" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "agreedToTermsAndConditions"`);
    }

}
