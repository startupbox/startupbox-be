import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveLockedPhases1632480993166 implements MigrationInterface {
    name = 'RemoveLockedPhases1632480993166'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "phase_entity" DROP COLUMN "unlocked"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "phase_entity" ADD "unlocked" boolean NOT NULL`);
    }

}
