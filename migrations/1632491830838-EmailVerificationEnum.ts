import {MigrationInterface, QueryRunner} from "typeorm";

export class EmailVerificationEnum1632491830838 implements MigrationInterface {
    name = 'EmailVerificationEnum1632491830838'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" RENAME COLUMN "isVerified" TO "emailVerification"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "emailVerification"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "emailVerification" character varying NOT NULL DEFAULT 'PENDING'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "emailVerification"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "emailVerification" boolean`);
        await queryRunner.query(`ALTER TABLE "user_entity" RENAME COLUMN "emailVerification" TO "isVerified"`);
    }

}
