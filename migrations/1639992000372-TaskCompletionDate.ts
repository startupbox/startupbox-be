import {MigrationInterface, QueryRunner} from "typeorm";

export class TaskCompletionDate1639992000372 implements MigrationInterface {
    name = 'TaskCompletionDate1639992000372'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" ADD "completedAt" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" DROP COLUMN "completedAt"`);
    }

}
