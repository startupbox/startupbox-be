import { MigrationInterface, QueryRunner } from 'typeorm';

export class DiaryRefactor1635756039833 implements MigrationInterface {
  name = 'DiaryRefactor1635756039833';

  async up(queryRunner: QueryRunner): Promise<void> {
    const dbTasks = await queryRunner.query(`SELECT task_entity."id", task_entity."pointer" FROM "task_entity"`);
    const rawData = await queryRunner.query(`SELECT phase_entity."diaryId" as diaryId, phase_entity.id as phaseId, component_entity."id" as componentId, task_entity.id as taskId FROM phase_entity
    LEFT JOIN component_entity ON component_entity."phaseId" = phase_entity."id"
    LEFT JOIN task_entity ON task_entity."componentId" = component_entity."id"
    `);

    const cmsTasksMap = {
      TASK_1: 'ckv880v48rnwm0c59p4bgek75',
      TASK_2: 'ckv881saolvxx0c50sd4unjzn',
      TASK_3: 'ckv8bw8v4pwvc0b56cb9eppzg',
      TASK_4: 'ckv8bwsxc6o1r0b494avl7g10',
      TASK_5: 'ckv8bxej4ufpx0c59bubp4780',
      TASK_6: 'ckv8bxxtki5to0b06qbmp8dk5',
      TASK_7: 'ckv8byinkuge80c59bavdfgrq',
      TASK_8: 'ckv8byvrsi6nv0b06qo6twovv',
      TASK_9: 'ckv8bzbz4otkv0d57sakuvck5',
      TASK_10: 'ckv8bzreouhv60c59rnml80hp',
      TASK_11: 'ckv8c08dsi7p30b06nhqez7qp',
      TASK_12: 'ckv8c0kq8uif00c59bvv78jj1',
      TASK_13: 'ckv8c10xki8gi0b06t3xhr4zw',
      TASK_14: 'ckv8c1gd4uj9f0c59o0hzmw5b',
      TASK_15: 'ckv8c1rxsq29w0b56vpd0m4ba',
      TASK_16: 'ckv8c2520q2mc0b56dntj647h',
      TASK_17: 'ckv8c2iy0ow740d575ofuiexu',
      TASK_18: 'ckv8c2w28q33b0b56a2ba4d6z',
      TASK_19: 'ckv8c39y8q3gj0b56hyf5pki1',
      TASK_20: 'ckv8c3kr4iair0b06w72709zj',
      TASK_21: 'ckv8c3x3kul1u0c59ggflcixd',
      TASK_22: 'ckv8c4a7sul9x0c59c9f90o5o',
      TASK_22_1: 'ckv8c4mk8ulhv0c59uphok0nb',
      TASK_23: 'ckv8c54b4ulsz0c59z1cyfdx8',
      TASK_24: 'ckv8c5gnkq4yz0b565w3c12qc',
      TASK_25: 'ckv8c5t00oybh0d57n2yt65bb',
      TASK_26: 'ckv8c67nsouoe0c50t0ffsurp',
      TASK_27: 'ckv8c6k086vgv0b49ug5jfamr',
      TASK_28: 'ckv8c6wcoidb40b0611v1kw3e',
      TASK_29: 'ckv8c76dsovbv0c501ws9jyfu',
      TASK_30: 'ckv8c7l1kovj90c50jsso8m1w',
      TASK_31: 'ckv8c7yxkovr30c50r96qwyoq',
      TASK_32: 'ckv8c8ba0q6it0b563gf7u1nk',
      TASK_33: 'ckv8c8oe8p05v0d57i1hzqmdv',
      TASK_34: 'ckv8c8zywq6v80b56fuol054b',
      TASK_35: 'ckv8c9ars6wx40b49tlggy4b9',
      TASK_36: 'ckv8c9mcgp0pm0d57bm671pbx',
      TASK_37: 'ckv8ca108ietd0b064dldpsdu',
      TASK_38: 'ckv8cae4giezx0b0630zf1nck',
      TASK_39: 'ckv8caqgwp1dg0d577zziih2y',
      TASK_40: 'ckv8cb21kq8b40b56yatpkbov',
      TASK_41: 'ckv8cbf5sq8l00b56h60vmalq',
      TASK_42: 'ckv8cbttk6y9f0b4932k6moum',
      TASK_43: 'ckv8cc6606yhh0b49a0k8v1ny',
      TASK_44: 'ckv8ccja8uqix0c5905d2jqwz',
      TASK_45: 'ckv8ccwegigmz0b06iq5s48sy',
      TASK_46: 'ckv8cd77cp2qk0d57422hrdgp',
      TASK_47: 'ckv8cdis0oywd0c50trv2fv0e',
      TASK_48: 'ckv8cdv4gp32e0d57qhofar3b',
      TASK_49: 'ckv8ce55kihdf0b061g9o7y2e',
      TASK_50: 'ckv8cei9sqarr0b56cxkvhx6s',
      TASK_51: 'ckv8cetug70uh0b49lql614s2',
      TASK_52: 'ckv8cf9a0711a0b49j454uvwk',
      TASK_53: 'ckv8cflmgiijz0b0610z6v2h1',
      TASK_54: 'ckv8cfvnkut400c592w4jd072',
    };

    await queryRunner.query(`ALTER TABLE "task_entity" DROP CONSTRAINT "FK_667945c8db91ea681c4d39017ef"`);
    await queryRunner.query(`ALTER TABLE "diary_entity" DROP CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4"`);
    await queryRunner.query(`ALTER TABLE "task_entity" DROP COLUMN "componentId"`);
    await queryRunner.query(`ALTER TABLE "task_entity" ADD "diaryId" uuid`);
    await queryRunner.query(`ALTER TABLE "diary_entity" DROP COLUMN "version"`);
    await queryRunner.query(`ALTER TABLE "project_entity" ADD "scenarioId" character varying DEFAULT 'ckv8dkbo8qyhp0b56op8cr6i8'`);
    await queryRunner.query(`ALTER TABLE "diary_entity" DROP CONSTRAINT "REL_c77dd5dab6e06736035b4c61ba"`);
    await queryRunner.query(
      `ALTER TABLE "task_entity" ADD CONSTRAINT "FK_594fcbbd623747131a58a07d2b5" FOREIGN KEY ("diaryId") REFERENCES "diary_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "diary_entity" ADD CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4" FOREIGN KEY ("projectId") REFERENCES "project_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );

    const diaryForTask = {};
    rawData.map((data) => (diaryForTask[data.taskid] = data.diaryid));

    dbTasks.forEach(async (task) => {
      await queryRunner.query(`UPDATE "task_entity" SET "diaryId" = '${diaryForTask[task.id]}' WHERE task_entity."id" = '${task.id}'`);
      await queryRunner.query(`UPDATE "task_entity" SET "pointer" = '${cmsTasksMap[task.pointer]}' WHERE task_entity."id" = '${task.id}'`);
    });
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "diary_entity" DROP CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4"`);
    await queryRunner.query(`ALTER TABLE "task_entity" DROP CONSTRAINT "FK_594fcbbd623747131a58a07d2b5"`);
    await queryRunner.query(`ALTER TABLE "diary_entity" ADD CONSTRAINT "REL_c77dd5dab6e06736035b4c61ba" UNIQUE ("projectId")`);
    await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "scenarioId"`);
    await queryRunner.query(`ALTER TABLE "diary_entity" ADD "version" character varying`);
    await queryRunner.query(`ALTER TABLE "task_entity" DROP COLUMN "diaryId"`);
    await queryRunner.query(`ALTER TABLE "task_entity" ADD "componentId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "diary_entity" ADD CONSTRAINT "FK_c77dd5dab6e06736035b4c61ba4" FOREIGN KEY ("projectId") REFERENCES "project_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "task_entity" ADD CONSTRAINT "FK_667945c8db91ea681c4d39017ef" FOREIGN KEY ("componentId") REFERENCES "component_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
  }
}
