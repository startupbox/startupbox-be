import {MigrationInterface, QueryRunner} from "typeorm";

export class IndexingUserId1594890263487 implements MigrationInterface {
    name = 'IndexingUserId1594890263487'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP CONSTRAINT "FK_9c0353760c806d01b6f61657a2c"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ALTER COLUMN "userId" SET NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_9c0353760c806d01b6f61657a2" ON "profile_entity" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_2d221fcb3ee0ff396c09d2e831" ON "identity_entity" ("userId") `);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD CONSTRAINT "FK_9c0353760c806d01b6f61657a2c" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP CONSTRAINT "FK_9c0353760c806d01b6f61657a2c"`);
        await queryRunner.query(`DROP INDEX "IDX_2d221fcb3ee0ff396c09d2e831"`);
        await queryRunner.query(`DROP INDEX "IDX_9c0353760c806d01b6f61657a2"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ALTER COLUMN "userId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD CONSTRAINT "FK_9c0353760c806d01b6f61657a2c" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
