import {MigrationInterface, QueryRunner} from "typeorm";

export class InitProjectEntity1594989283763 implements MigrationInterface {
    name = 'InitProjectEntity1594989283763'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "project_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "targetAudience" character varying NOT NULL, "industry" character varying NOT NULL, "phase" character varying NOT NULL, "businessType" character varying NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_7a75a94e01d0b50bff123db1b87" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ea4982b0ce8cb41f951c0954cb" ON "project_entity" ("userId") `);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD CONSTRAINT "FK_ea4982b0ce8cb41f951c0954cbd" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP CONSTRAINT "FK_ea4982b0ce8cb41f951c0954cbd"`);
        await queryRunner.query(`DROP INDEX "IDX_ea4982b0ce8cb41f951c0954cb"`);
        await queryRunner.query(`DROP TABLE "project_entity"`);
    }

}
