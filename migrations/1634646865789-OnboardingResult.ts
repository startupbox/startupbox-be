import {MigrationInterface, QueryRunner} from "typeorm";

export class OnboardingResult1634646865789 implements MigrationInterface {
    name = 'OnboardingResult1634646865789'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "onboardingResult" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "onboardingResult"`);
    }

}
