import {MigrationInterface, QueryRunner} from "typeorm";

export class ProjectCreationAdjustments1638523350286 implements MigrationInterface {
    name = 'ProjectCreationAdjustments1638523350286'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "industry" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "scenarioId" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" SET DEFAULT 'EMAIL'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "refresh_token_entity" ALTER COLUMN "provider" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "scenarioId" SET DEFAULT 'ckv8dkbo8qyhp0b56op8cr6i8'`);
        await queryRunner.query(`ALTER TABLE "project_entity" ALTER COLUMN "industry" SET NOT NULL`);
    }

}
