import {MigrationInterface, QueryRunner} from "typeorm";

export class Subdomain1639471672582 implements MigrationInterface {
    name = 'Subdomain1639471672582'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "subdomain" character varying NOT NULL DEFAULT 'default'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "subdomain"`);
    }

}
