import {MigrationInterface, QueryRunner} from "typeorm";

export class UserRoleEnum1595409743925 implements MigrationInterface {
    name = 'UserRoleEnum1595409743925'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "role" character varying NOT NULL DEFAULT 'CLIENT'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "role"`);
    }

}
