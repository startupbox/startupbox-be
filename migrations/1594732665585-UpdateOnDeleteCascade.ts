import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateOnDeleteCascade1594732665585 implements MigrationInterface {
    name = 'UpdateOnDeleteCascade1594732665585'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "identity_entity" DROP CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317"`);
        await queryRunner.query(`ALTER TABLE "identity_entity" ADD CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "identity_entity" DROP CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317"`);
        await queryRunner.query(`ALTER TABLE "identity_entity" ADD CONSTRAINT "FK_2d221fcb3ee0ff396c09d2e8317" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
