import {MigrationInterface, QueryRunner} from "typeorm";

export class RefactorTaskPointerEnumeration1596310372432 implements MigrationInterface {
    name = 'RefactorTaskPointerEnumeration1596310372432'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" RENAME COLUMN "title" TO "pointer"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task_entity" RENAME COLUMN "pointer" TO "title"`);
    }

}
