import {MigrationInterface, QueryRunner} from "typeorm";

export class Onboarding1632237817623 implements MigrationInterface {
    name = 'Onboarding1632237817623'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP COLUMN "experience"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "onboardingData" jsonb`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "onboardingData"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD "experience" character varying NOT NULL`);
    }

}
