import {MigrationInterface, QueryRunner} from "typeorm";

export class ProjectAndDiaryEdit1634814221546 implements MigrationInterface {
    name = 'ProjectAndDiaryEdit1634814221546'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "diary_entity" ADD "archived" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "project_entity" ADD "archived" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_entity" DROP COLUMN "archived"`);
        await queryRunner.query(`ALTER TABLE "diary_entity" DROP COLUMN "archived"`);
    }

}
