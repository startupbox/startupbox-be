import {MigrationInterface, QueryRunner} from "typeorm";

export class TransferProfileUserOwnership1594889636011 implements MigrationInterface {
    name = 'TransferProfileUserOwnership1594889636011'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP CONSTRAINT "FK_861abf5759299b717c5e2780a78"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP CONSTRAINT "UQ_861abf5759299b717c5e2780a78"`);
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "profileId"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD "userId" uuid`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD CONSTRAINT "UQ_9c0353760c806d01b6f61657a2c" UNIQUE ("userId")`);
        await queryRunner.query(`ALTER TABLE "profile_entity" ADD CONSTRAINT "FK_9c0353760c806d01b6f61657a2c" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP CONSTRAINT "FK_9c0353760c806d01b6f61657a2c"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP CONSTRAINT "UQ_9c0353760c806d01b6f61657a2c"`);
        await queryRunner.query(`ALTER TABLE "profile_entity" DROP COLUMN "userId"`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "profileId" uuid`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD CONSTRAINT "UQ_861abf5759299b717c5e2780a78" UNIQUE ("profileId")`);
        await queryRunner.query(`ALTER TABLE "user_entity" ADD CONSTRAINT "FK_861abf5759299b717c5e2780a78" FOREIGN KEY ("profileId") REFERENCES "profile_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
