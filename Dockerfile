FROM node:20.11.0
EXPOSE 3000
COPY . /home/app
WORKDIR /home/app
RUN yarn install
CMD ["yarn", "start:dev-with-db"]