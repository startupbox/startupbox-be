import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BasicType {
  @Field(() => ID, { nullable: true })
  id: string;

  @Field({ nullable: true })
  createdAt: Date;

  @Field({ nullable: true })
  updatedAt: Date;
}
