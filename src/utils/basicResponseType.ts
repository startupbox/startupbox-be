import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class BasicResponseType {
  @Field()
  message: string;

  @Field()
  code: string;

  @Field()
  success: boolean;
}
