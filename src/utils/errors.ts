export class UserAlreadyExists extends Error {}

export class InvalidVerificationToken extends Error {}
