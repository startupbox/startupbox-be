/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable func-style */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { configService } from './config/config.service';
import { ConfigKeys } from './config/configKeys.enum';
import { Logger, ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = Number.parseInt(configService.get(ConfigKeys.PORT, '3000'));

  app.use(
    helmet(
      configService.isProduction()
        ? {}
        : {
            crossOriginEmbedderPolicy: false,
            contentSecurityPolicy: {
              directives: {
                imgSrc: [`'self'`, 'data:', 'apollo-server-landing-page.cdn.apollographql.com'],
                scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
                manifestSrc: [`'self'`, 'apollo-server-landing-page.cdn.apollographql.com'],
                frameSrc: [`'self'`, 'sandbox.embed.apollographql.com'],
              },
            },
          }
    )
  );
  app.enableCors(configService.createCorsOptions());
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(port);
  Logger.log(`Server listening on port ${port}`);
}
bootstrap();
