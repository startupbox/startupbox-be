/* eslint-disable no-process-env */
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { ConfigKeys } from './configKeys.enum';
import { join } from 'path';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { GraphcmsStage } from '../components/diary/interfaces/stage.enum';
import { ApolloDriverConfig } from '@nestjs/apollo';
import { DataSourceOptions } from 'typeorm';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';

dotenv.config();

export class ConfigService {
  private readonly envConfig: { [key: string]: string };

  constructor(
    filePath: string = process.env.NODE_ENV === 'production' ? '.env' : process.env.NODE_ENV === 'test' ? '.env.test' : '.env.development'
  ) {
    try {
      this.envConfig = dotenv.parse(fs.readFileSync(filePath));
    } catch (error) {
      if (error.code === 'ENOENT') {
        this.envConfig = {};
        return;
      }
      throw error;
    }
  }

  get(key: ConfigKeys, defaultValue?: string): string | undefined {
    return process.env[key] || this.envConfig[key] || defaultValue;
  }

  isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
  }

  isStaging(): boolean {
    return process.env.NODE_ENV === 'staging';
  }

  isDevelopment(): boolean {
    return process.env.NODE_ENV === 'development';
  }

  isTest(): boolean {
    return process.env.NODE_ENV === 'test';
  }

  accessTokenExpiresIn(): number {
    return Number(process.env.JWT_ACCESS_TKN_AGE) || 604800;
  }

  refreshTokenExpiresIn(): number {
    return Number(process.env.JWT_REFRESH_TKN_AGE) || 864000000;
  }

  // TODO: sort origin requests
  createCorsOptions(): CorsOptions {
    return {
      credentials: true,
      origin: configService.isProduction()
        ? /https?:\/\/([a-z0-9-]+[.])*startupbox[.]app/
        : /(?:^|[ \t])((https?:\/\/)?(?:localhost|[\w-]+(?:\.[\w-]+)+)(:\d+)?(\/\S*)?)/,
    };
  }

  createGqlOptions(): ApolloDriverConfig {
    return {
      plugins: this.isProduction() ? [] : [ApolloServerPluginLandingPageLocalDefault()],
      playground: false,
      autoSchemaFile: join(process.cwd(), 'gql/schema.gql'),
      path: '/api/graphql',
    };
  }

  createTypeOrmOptions(): TypeOrmModuleOptions & DataSourceOptions {
    return {
      type: 'postgres',
      url: this.get(ConfigKeys.DATABASE_URL, undefined),
      host: this.get(ConfigKeys.DB_HOST, 'localhost'),
      port: Number(this.get(ConfigKeys.DB_PORT, '5432')),
      username: this.get(ConfigKeys.DB_USER, 'postgres'),
      password: this.get(ConfigKeys.DB_PASSWORD, undefined),
      database: this.get(ConfigKeys.DB_NAME, 'sbx_dev'),
      entities: [join(__dirname, '/../**/*.entity.{js,ts}')],
      migrations: [join(__dirname, '..', '..', 'migrations', '*.{js,ts}')],
      autoLoadEntities: true,
      dropSchema: this.isTest(),
      synchronize: this.isTest(),
      ssl: this.get(ConfigKeys.DB_SSL, 'enabled') === 'enabled' && {
        rejectUnauthorized: false,
      },
      migrationsTransactionMode: 'all',
    };
  }

  defaultCmsStage(): string {
    return this.isProduction() ? GraphcmsStage.PUBLISHED : GraphcmsStage.DRAFT;
  }
}

const configService = new ConfigService();

export { configService };
