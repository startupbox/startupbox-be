export enum ConfigKeys {
  NODE_ENV = 'NODE_ENV',
  PORT = 'PORT',
  FE_PORT = 'FE_PORT',
  BE_BASE_URL = 'BE_BASE_URL',
  FE_BASE_URL = 'FE_BASE_URL',
  DB_HOST = 'DB_HOST',
  DB_PORT = 'DB_PORT',
  DB_USER = 'DB_USER',
  DB_PASSWORD = 'DB_PASSWORD',
  DB_NAME = 'DB_NAME',
  DB_SSL = 'DB_SSL',
  CLIENT_ID_LINKEDIN = 'CLIENT_ID_LINKEDIN',
  CLIENT_SECRET_LINKEDIN = 'CLIENT_SECRET_LINKEDIN',
  JWT_ACCESS_PUBLIC_KEY = 'JWT_REFRESH_PUBLIC_KEY',
  JWT_ACCESS_PRIVATE_KEY = 'JWT_REFRESH_PRIVATE_KEY',
  JWT_REFRESH_PUBLIC_KEY = 'JWT_REFRESH_PUBLIC_KEY',
  JWT_REFRESH_PRIVATE_KEY = 'JWT_REFRESH_PRIVATE_KEY',
  JWT_REFRESH_TKN_AGE = 'JWT_REFRESH_TKN_AGE',
  MAILGUN_DOMAIN = 'MAILGUN_DOMAIN',
  MAILGUN_API_KEY = 'MAILGUN_API_KEY',
  JWT_OTHER_PRIVATE_KEY = 'JWT_OTHER_PRIVATE_KEY',
  CLIENT_ID_FACEBOOK = 'CLIENT_ID_FACEBOOK',
  CLIENT_SECRET_FACEBOOK = 'CLIENT_SECRET_FACEBOOK',
  CLIENT_ID_GOOGLE = 'CLIENT_ID_GOOGLE',
  CLIENT_SECRET_GOOGLE = 'CLIENT_SECRET_GOOGLE',
  GRAPHCMS_TOKEN = 'GRAPHCMS_TOKEN',
  GRAPHCMS_API_URL = 'GRAPHCMS_API_URL',
  DATABASE_URL = 'DATABASE_URL',
  ECOMAIL_API_KEY = 'ECOMAIL_API_KEY',
  ECOMAIL_BASE_URL = 'ECOMAIL_BASE_URL',
  ECOMAIL_SUBSCRIBERS_LIST_ID = 'ECOMAIL_SUBSCRIBERS_LIST_ID',
  MAILCHIMP_SERVER = 'MAILCHIMP_SERVER',
  MAILCHIMP_API_KEY = 'MAILCHIMP_API_KEY',
  MAILCHIMP_SUBSCRIBERS_LIST_ID = 'MAILCHIMP_SUBSCRIBERS_LIST_ID',
}
