import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComponentsModule } from './components/components.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { ApolloDriver } from '@nestjs/apollo';

@Module({
  imports: [
    ConfigModule,
    ComponentsModule,
    GraphQLModule.forRootAsync({ useClass: ConfigService, driver: ApolloDriver }),
    TypeOrmModule.forRootAsync({ useClass: ConfigService }),
  ],
})
export class AppModule {}
