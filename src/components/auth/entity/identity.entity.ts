import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, Column, ManyToOne, JoinColumn, Index } from 'typeorm';
import { UserEntity } from './user.entity';
import { AuthProvider } from '../interfaces/authProvider.enum';

@Entity()
export class IdentityEntity extends BasicEntity {
  @Column('text')
  provider: AuthProvider;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.identities, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;

  @Index()
  @Column()
  userId: string;
}
