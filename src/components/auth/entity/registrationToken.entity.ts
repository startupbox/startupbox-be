import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn } from 'typeorm';
import moment from 'moment';

@Entity()
export class RegistrationTokenEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  verificationToken: string;

  @Column()
  password: string;

  @Column()
  email: string;

  @Column({ default: false })
  used: boolean;

  @CreateDateColumn()
  createdAt: Date;

  get validUntil(): Date {
    return moment(this.createdAt).add(1, 'days').toDate();
  }
}
