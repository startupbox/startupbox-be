import { PrimaryGeneratedColumn, Entity, CreateDateColumn, ManyToOne, Column } from 'typeorm';
import { UserEntity } from './user.entity';

@Entity()
export class ResetPasswordTokenEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @ManyToOne(() => UserEntity, { eager: true })
  user: UserEntity;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ nullable: true })
  validUntil: Date;
}
