import { Entity, Column, OneToMany, Unique, OneToOne } from 'typeorm';
import { BasicEntity } from '../../../utils/basicEntity';
import { IdentityEntity } from './identity.entity';
import { ProfileEntity } from './profile.entity';
import { ProjectEntity } from '../../project/entity/project.entity';
import { CommentEntity } from '../../../components/diary/entity/comment.entity';
import { UserRole } from '../interfaces/userRole.enum';
import { RefreshTokenEntity } from './refreshToken.entity';
import { OnboardingType } from '../type/onboardingData.type';
import { EmailVerification } from '../interfaces/emailVerification.enum';
import { ActivityEntity } from '../../activity/entity/activity.entity';

@Entity()
@Unique(['email'])
export class UserEntity extends BasicEntity {
  @Column({ nullable: true })
  firstName?: string;

  @Column({ nullable: true })
  lastName?: string;

  @Column({ nullable: true })
  email?: string;

  @Column({ nullable: true })
  profilePicture?: string;

  @Column('text', {
    default: UserRole.CLIENT,
  })
  role: UserRole;

  @OneToMany(() => IdentityEntity, (identity: IdentityEntity) => identity.user)
  identities: IdentityEntity[];

  @OneToOne(() => ProfileEntity, (profile: ProfileEntity) => profile.user, { nullable: true })
  profile?: ProfileEntity;

  @OneToMany(() => ProjectEntity, (project: ProjectEntity) => project.user, { nullable: true })
  projects?: ProjectEntity[];

  @OneToMany(() => CommentEntity, (comment: CommentEntity) => comment.user, { nullable: true })
  comments?: CommentEntity[];

  @OneToMany(() => ActivityEntity, (activity: ActivityEntity) => activity.user, { nullable: true })
  activityLog?: ActivityEntity[];

  @Column('text', { default: EmailVerification.PENDING })
  emailVerification?: EmailVerification;

  @Column({ nullable: true })
  password?: string;

  @OneToMany(() => RefreshTokenEntity, (refreshToken) => refreshToken.user, { nullable: true })
  refreshTokens?: RefreshTokenEntity[];

  @Column('jsonb', { nullable: true })
  onboardingData?: OnboardingType[];

  @Column({ default: false })
  agreedToTermsAndConditions: boolean;

  @Column({ nullable: true })
  onboardingResult?: string;

  @Column({ default: false })
  archived: boolean;

  @Column({ default: 'default' })
  subdomain: string;
}
