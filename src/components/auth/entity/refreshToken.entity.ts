import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { AuthProvider } from '../interfaces/authProvider.enum';
import { UserEntity } from './user.entity';

@Entity()
export class RefreshTokenEntity {
  @PrimaryColumn('varchar')
  id: string;

  @ManyToOne(() => UserEntity, (user) => user.refreshTokens, { eager: true })
  user: UserEntity;

  @Column('boolean', { default: false })
  revoked: boolean;

  @Column('text', { default: AuthProvider.EMAIL })
  provider: AuthProvider;
}
