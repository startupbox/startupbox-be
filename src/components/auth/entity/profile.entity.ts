import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, Column, OneToOne, JoinColumn, Index } from 'typeorm';
import { UserEntity } from './user.entity';

@Entity()
export class ProfileEntity extends BasicEntity {
  @Column()
  region: string;

  @Column('simple-array', { nullable: true })
  skills: string[];

  @OneToOne(() => UserEntity, (user: UserEntity) => user.profile, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;

  @Index()
  @Column()
  userId: string;
}
