import { Test, TestingModule } from '@nestjs/testing';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { userFactory } from '../../../test/factories/user.factory';
import { ConfigService } from '../../config/config.service';
import { CmsService } from '../cms/cms.service';
import { ActivityService } from '../activity/activity.service';

const mockAuthService = () => ({
  getUser: jest.fn(),
});

const mockConfigService = () => ({
  accessTokenExpiresIn: jest.fn(),
  refreshTokenExpiresIn: jest.fn(),
});

const mockCmsService = () => ({});

const mockActivityService = () => ({
  createActivity: jest.fn(),
});

describe('AuthResolver', () => {
  let resolver: AuthResolver;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let service: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let cmsService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthResolver,
        {
          provide: AuthService,
          useFactory: mockAuthService,
        },
        {
          provide: ConfigService,
          useFactory: mockConfigService,
        },
        {
          provide: CmsService,
          useFactory: mockCmsService,
        },
        {
          provide: ActivityService,
          useFactory: mockActivityService,
        },
      ],
    }).compile();

    resolver = module.get<AuthResolver>(AuthResolver);
    service = module.get<AuthService>(AuthService);
    cmsService = module.get<CmsService>(CmsService);
  });

  it('resolver should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  it('cmsService should be defined', () => {
    expect(cmsService).toBeDefined();
  });

  describe('queries', () => {
    it('should return a user', async () => {
      const user = userFactory();
      service.getUser.mockResolvedValue(user);
      const result = await resolver.getUser(user);
      expect(result).toEqual(user);
    });
  });
});
