import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';
import { ActivityService } from '../activity/activity.service';
import { ActivityEntity } from '../activity/entity/activity.entity';
import { ActivityRepository } from '../activity/repository/activity.repository';
import { CmsModule } from '../cms/cms.module';
import { DiaryModule } from '../diary/diary.module';
import { EcomailService } from '../ecomail/ecomail.service';
import { EmailService } from '../email/email.service';
import { MailchimpService } from '../mailchimp/mailchimp.service';
import { ProjectEntity } from '../project/entity/project.entity';
import { ProjectModule } from '../project/project.module';
import { ProjectService } from '../project/project.service';
import { ProjectRepository } from '../project/repository/project.repository';
import { AuthController } from './auth.controller';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { jwtConfig } from './config/jwt.config';
import { passportConfig } from './config/passport.config';
import { IdentityEntity } from './entity/identity.entity';
import { ProfileEntity } from './entity/profile.entity';
import { RefreshTokenEntity } from './entity/refreshToken.entity';
import { RegistrationTokenEntity } from './entity/registrationToken.entity';
import { ResetPasswordTokenEntity } from './entity/resetPasswordToken.entity';
import { UserEntity } from './entity/user.entity';
import { IdentityRepository } from './repository/identity.repository';
import { ProfileRepository } from './repository/profile.repository';
import { RefreshTokenRepository } from './repository/refreshToken.repository';
import { RegistrationTokenRepository } from './repository/registrationToken.repository';
import { ResetPasswordTokenRepository } from './repository/resetPasswordToken.repository';
import { UserRepository } from './repository/user.repository';
import { FacebookStrategy } from './strategy/facebook.strategy';
import { GoogleStrategy } from './strategy/google.strategy';
import { JwtStrategy } from './strategy/jwt.strategy';
import { LinkedInStrategy } from './strategy/linkedin.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      IdentityEntity,
      ProfileEntity,
      RegistrationTokenEntity,
      RefreshTokenEntity,
      ResetPasswordTokenEntity,
      ProjectEntity,
      ActivityEntity,
    ]),
    PassportModule.register(passportConfig),
    JwtModule.register(jwtConfig),
    ConfigModule,
    ProjectModule,
    DiaryModule,
    CmsModule,
  ],
  providers: [
    AuthResolver,
    AuthService,
    LinkedInStrategy,
    JwtStrategy,
    GoogleStrategy,
    FacebookStrategy,
    EmailService,
    ConfigService,
    ProjectService,
    EcomailService,
    MailchimpService,
    UserRepository,
    IdentityRepository,
    ProfileRepository,
    RegistrationTokenRepository,
    RefreshTokenRepository,
    ResetPasswordTokenRepository,
    ProjectRepository,
    ActivityRepository,
    ActivityService,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
