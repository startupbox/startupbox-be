import { registerEnumType } from '@nestjs/graphql';

export enum AuthProvider {
  LINKEDIN = 'LINKEDIN',
  EMAIL = 'EMAIL',
  FACEBOOK = 'FACEBOOK',
  GOOGLE = 'GOOGLE',
}

registerEnumType(AuthProvider, {
  name: 'AuthProvider',
});
