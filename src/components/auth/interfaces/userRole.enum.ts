import { registerEnumType } from '@nestjs/graphql';

export enum UserRole {
  CLIENT = 'CLIENT',
  ADMIN = 'ADMIN',
  SUPER_ADMIN = 'SUPER_ADMIN',
}

registerEnumType(UserRole, { name: 'UserRole' });
