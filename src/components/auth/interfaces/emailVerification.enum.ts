import { registerEnumType } from '@nestjs/graphql';

export enum EmailVerification {
  PENDING = 'PENDING',
  DONE = 'DONE',
  NOT_NEEDED = 'NOT_NEEDED',
}

registerEnumType(EmailVerification, {
  name: 'EmailVerification',
});
