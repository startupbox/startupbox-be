import { TokenType } from './tokenType.enum';
import { AuthProvider } from './authProvider.enum';

export interface TokenPayload {
  userId: string;
  type: TokenType;
  provider: AuthProvider;
  iat?: number;
  nbf?: number;
  exp?: number;
  jti?: string;
}
