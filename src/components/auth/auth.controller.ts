import { Controller, Get, UseGuards, Res, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import { IdentityEntity } from './entity/identity.entity';
import { getBaseUrl } from './utils/domain.util';
import { getServerBaseUri } from './utils/uri.utils';

@Controller('/api/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('/linkedin')
  @UseGuards(AuthGuard('linkedin'))
  async getLinkedInAuth(): Promise<void> {
    // start linkedin flow
    return;
  }

  @Get('/linkedin/callback')
  @UseGuards(AuthGuard('linkedin'))
  async callbackLinkedIn(@Req() req: Request, @Res() res: Response): Promise<void> {
    const { accessToken, refreshToken } = await this.authService.getCredentials(req.user as IdentityEntity);
    const domain = req.query.state ? req.query.state : getServerBaseUri(true);
    await this.authService.editUserSubdomain(req.user['userId'], domain);
    return this.sendCredentials(res, accessToken, refreshToken, domain);
  }

  @Get('/google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(): Promise<void> {
    // start google auth flow
    return;
  }

  @Get('/google/callback')
  @UseGuards(AuthGuard('google'))
  async googleAuthCallback(@Req() req: Request, @Res() res: Response): Promise<void> {
    const { accessToken, refreshToken } = await this.authService.getCredentials(req.user as IdentityEntity);
    const domain = req.query.state ? req.query.state : getServerBaseUri(true);
    await this.authService.editUserSubdomain(req.user['userId'], domain);
    return this.sendCredentials(res, accessToken, refreshToken, domain);
  }

  @Get('/facebook')
  @UseGuards(AuthGuard('facebook'))
  async facebookAuth(): Promise<void> {
    // start facebook auth flow
    return;
  }

  @Get('/facebook/callback')
  @UseGuards(AuthGuard('facebook'))
  async facebookCallback(@Req() req: Request, @Res() res: Response): Promise<void> {
    const { accessToken, refreshToken } = await this.authService.getCredentials(req.user as IdentityEntity);
    const domain = req.query.state ? req.query.state : getServerBaseUri(true);
    await this.authService.editUserSubdomain(req.user['userId'], domain);
    return this.sendCredentials(res, accessToken, refreshToken, domain);
  }

  private sendCredentials(res: Response, accessToken: string, refreshToken: string, domain?: any): void {
    const baseUrl = getBaseUrl(domain);
    res.status(302).redirect(`${baseUrl}/auth/${accessToken}/${refreshToken}`);
  }
}
