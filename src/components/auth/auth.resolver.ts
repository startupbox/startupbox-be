import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { UserType } from './type/user.type';
import { ProfileType } from './type/profile.type';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from './utils/gqlAuth.guard';
import { GetUser } from './utils/getUser.decorator';
import { UserEntity } from './entity/user.entity';
import { ProfileEntity } from './entity/profile.entity';
import { CreateUserProfileInput } from './inputs/createUserProfile.input';
import { RegistrationResponseType } from './type/registrationResponse.type';
import { RegisterInputType } from './inputs/registration.input';
import { ConfigService } from '../../config/config.service';
import { LoginResponseType } from './type/loginResponse.type';
import { VerifyUserInputType } from './inputs/verifyUser.input';
import { BasicResponseType } from '../../utils/basicResponseType';
import { ResetVerificationInput } from './inputs/resendVerificationEmail.input';
import { LoginInput } from './inputs/login.input';
import { RefreshAccessTokenInput } from './inputs/refreshToken.input';
import { InvalidVerificationToken, UserAlreadyExists } from '../../utils/errors';
import { ProfileResponseType } from './type/profileResponse.type';
import { OnboardingInput } from './inputs/onboarding.input';
import { ResetPasswordMutationResponseType } from './type/resetPasswordResponse';
import { ResetPasswordInputType } from './inputs/resetPassword.input';
import { EditUserProfileInput } from './inputs/editUserProfile.input';
import { CmsService } from '../cms/cms.service';
import { ActivityService } from '../activity/activity.service';

@Resolver(() => UserType)
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
    private readonly cmsService: CmsService,
    private readonly activityService: ActivityService
  ) {}

  @Query(() => UserType)
  @UseGuards(GqlAuthGuard)
  async getUser(@GetUser() user: UserEntity): Promise<UserEntity> {
    const entity = this.authService.getUser(user.id);
    await this.activityService.createActivity(user, 'getUser');
    return entity;
  }

  @Query(() => ProfileType)
  @UseGuards(GqlAuthGuard)
  async getProfile(@GetUser() user: UserEntity): Promise<ProfileEntity> {
    return this.authService.getProfile(user.id);
  }

  @Mutation(() => ProfileResponseType)
  @UseGuards(GqlAuthGuard)
  async createUserProfile(
    @GetUser() user: UserEntity,
    @Args('createUserProfileInput') createUserProfileInput: CreateUserProfileInput
  ): Promise<BasicResponseType | ProfileResponseType> {
    try {
      const { region, skills, firstName, lastName, profilePicture } = createUserProfileInput;
      const profile = await this.authService.createUserProfile(user.id, region, firstName, lastName, skills, profilePicture);
      return {
        code: '201',
        success: true,
        message: 'User profile successfully created.',
        profile,
      };
    } catch (e) {
      return {
        code: '409',
        success: false,
        message: 'User with this profile already exists.',
      };
    }
  }

  @Mutation(() => RegistrationResponseType)
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async register(
    @Args('data') data: RegisterInputType,
    @Context() context: any
  ): Promise<Omit<RegistrationResponseType, 'user'> & { user?: UserEntity }> {
    let requestId: string;
    let verificationToken: string;
    let accessToken: string;
    let refreshToken: string;
    let user: UserEntity;
    try {
      const domain = context.req.headers.origin;
      const response = await this.authService.register(data, domain);
      verificationToken = response.token.verificationToken;
      requestId = response.token.id;
      accessToken = response.accessToken;
      refreshToken = response.refreshToken;
      user = response.user;
    } catch (error) {
      if (error instanceof UserAlreadyExists) {
        return {
          code: '409',
          success: false,
          message: 'User with this email already exists',
        };
      } else
        return {
          code: '409',
          success: false,
          message: 'Something went wrong',
        };
    }

    return {
      code: '201',
      success: true,
      message: 'Successfully registered and confirmation email sent',
      requestId,
      verificationToken,
      accessToken,
      refreshToken,
      expiresIn: this.configService.accessTokenExpiresIn(),
      refreshExpiresIn: this.configService.refreshTokenExpiresIn(),
      user,
    };
  }

  @Mutation(() => LoginResponseType)
  async verifyUser(@Args('data') data: VerifyUserInputType): Promise<Omit<LoginResponseType, 'user'> & { user?: UserEntity }> {
    let accessToken: string;
    let refreshToken: string;
    let user: UserEntity;

    try {
      const verificationData = await this.authService.verifyUser(data);
      user = verificationData.user;
      accessToken = verificationData.accessToken;
      refreshToken = verificationData.refreshToken;
    } catch (error) {
      if (error instanceof InvalidVerificationToken) {
        return {
          code: '403',
          success: false,
          message: 'Invalid verification token',
        };
      } else throw error;
    }

    return {
      code: '201',
      success: true,
      message: 'User email verified.',
      expiresIn: this.configService.accessTokenExpiresIn(),
      refreshExpiresIn: this.configService.refreshTokenExpiresIn(),
      tokenType: 'Bearer',
      accessToken,
      refreshToken,
      user,
    };
  }

  @Mutation(() => BasicResponseType)
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async resendVerficationEmail(@Args('data') data: ResetVerificationInput, @Context() context: any): Promise<BasicResponseType> {
    try {
      const domain = context.req.headers.origin;
      await this.authService.resendVerificationEmail(data, domain);
      return {
        code: '201',
        success: true,
        message: 'Confirmation email resent',
      };
    } catch (e) {
      return {
        code: '400',
        success: false,
        message: 'Trouble sending confirmation email.',
      };
    }
  }

  @Mutation(() => LoginResponseType)
  async login(@Args('data') data: LoginInput): Promise<(Omit<LoginResponseType, 'user'> & { user: UserEntity }) | BasicResponseType> {
    try {
      const { accessToken, refreshToken, user } = await this.authService.login(data);
      return {
        code: '200',
        success: true,
        message: 'Successfully logged in.',
        expiresIn: this.configService.accessTokenExpiresIn(),
        refreshExpiresIn: this.configService.refreshTokenExpiresIn(),
        tokenType: 'Bearer',
        accessToken,
        refreshToken,
        user,
      };
    } catch (error) {
      return {
        code: '403',
        success: false,
        message: 'Invalid login',
      };
    }
  }

  @Mutation(() => LoginResponseType)
  async refreshAccessToken(@Args('data') data: RefreshAccessTokenInput): Promise<Omit<LoginResponseType, 'user'> & { user: UserEntity }> {
    const { accessToken, refreshToken: newRefreshtoken, user } = await this.authService.refreshAccessToken(data.refreshToken, data.userId);

    return {
      code: '201',
      success: true,
      message: 'Successfully refreshed access token.',
      expiresIn: this.configService.accessTokenExpiresIn(),
      refreshExpiresIn: this.configService.refreshTokenExpiresIn(),
      tokenType: 'Bearer',
      accessToken,
      refreshToken: newRefreshtoken,
      user,
    };
  }

  @Mutation(() => UserType)
  @UseGuards(GqlAuthGuard)
  async onboarding(@GetUser() user: UserEntity, @Args('data') data: OnboardingInput): Promise<UserEntity> {
    return await this.authService.saveOnboardingData(user, data, user.subdomain);
  }

  @Mutation(() => ResetPasswordMutationResponseType)
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async requestPasswordReset(@Args('email') email: string, @Context() context: any): Promise<ResetPasswordMutationResponseType> {
    try {
      const domain = context.req.headers.origin;
      const token = await this.authService.requestPasswordReset(email, domain);

      return {
        success: true,
        message: 'Sent password reset email.',
        code: '200',
        requestId: token.id || null,
        verificationCode: token.code || null,
      };
    } catch (e) {
      return {
        success: false,
        message: 'Unable to send password reset email.',
        code: '500',
      };
    }
  }

  @Mutation(() => BasicResponseType)
  async resetPassword(@Args('data') data: ResetPasswordInputType): Promise<BasicResponseType> {
    try {
      await this.authService.resetPassword(data);
      return {
        success: true,
        message: 'Password successfully reset.',
        code: '200',
      };
    } catch (e) {
      return {
        success: false,
        message: 'Unable to reset password.',
        code: '500',
      };
    }
  }

  @Mutation(() => BasicResponseType)
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async resendPasswordResetEmail(@Args('requestId') requestId: string, @Context() context: any): Promise<BasicResponseType> {
    const domain = context.req.headers.origin;
    await this.authService.resendResetPasswordEmail(requestId, domain);

    return {
      code: '201',
      success: true,
      message: 'Reset password email resent',
    };
  }

  @Mutation(() => ProfileResponseType)
  @UseGuards(GqlAuthGuard)
  async editUserProfile(
    @GetUser() user: UserEntity,
    @Args('editProfileInput') editProfileInput: EditUserProfileInput
  ): Promise<ProfileResponseType> {
    try {
      const profile = await this.authService.editUserProfile(user.id, editProfileInput);
      return {
        message: 'User profile edited sucessfully.',
        code: '201',
        success: true,
        profile,
      };
    } catch (e) {
      return {
        message: 'Something went wrong while editing user profile',
        code: '500',
        success: false,
      };
    }
  }

  @Mutation(() => BasicResponseType)
  @UseGuards(GqlAuthGuard)
  async deleteUserProfile(@GetUser() user: UserEntity): Promise<BasicResponseType> {
    try {
      await this.cmsService.deleteOldImageFromCms(user.profilePicture);
      await this.authService.deleteUserProfile(user.id);
      return {
        message: 'User profile deleted successfully.',
        success: true,
        code: '200',
      };
    } catch (e) {
      return {
        message: 'Something went wrong while deleting user profile.',
        success: false,
        code: '500',
      };
    }
  }
}
