import {
  Injectable,
  InternalServerErrorException,
  OnApplicationBootstrap,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from './repository/user.repository';
import { IdentityRepository } from './repository/identity.repository';
import { ProfileRepository } from './repository/profile.repository';
import { IdentityEntity } from './entity/identity.entity';
import { UserEntity } from './entity/user.entity';
import { ProfileEntity } from './entity/profile.entity';
import { configService } from '../../config/config.service';
import { ConfigKeys } from '../../config/configKeys.enum';
import { AuthProvider } from './interfaces/authProvider.enum';
import { TokenType } from './interfaces/tokenType.enum';
import LRUCache from 'lru-cache';
import * as uuid from 'uuid';
import { RegistrationTokenEntity } from './entity/registrationToken.entity';
import { RegistrationTokenRepository } from './repository/registrationToken.repository';
import { EmailService } from '../email/email.service';
import { VerifyUserInputType } from './inputs/verifyUser.input';
import { RefreshTokenEntity } from './entity/refreshToken.entity';
import { RefreshTokenRepository } from './repository/refreshToken.repository';
import { hashPassword } from './utils/hashPassword';
import { InvalidVerificationToken, UserAlreadyExists } from '../../../src/utils/errors';
import { ResetPasswordTokenEntity } from './entity/resetPasswordToken.entity';
import { ResetPasswordTokenRepository } from './repository/resetPasswordToken.repository';
import { ResetPasswordInputType } from './inputs/resetPassword.input';
import { EmailVerification } from './interfaces/emailVerification.enum';
import { OnboardingInput } from './inputs/onboarding.input';
import { EditUserProfileInput } from './inputs/editUserProfile.input';
import { DataSource } from 'typeorm';
import { ProjectService } from '../project/project.service';
import { CmsService } from '../cms/cms.service';
import { getSubdomainFromUrl } from './utils/domain.util';
import { EcomailService } from '../ecomail/ecomail.service';
import { MailchimpService } from '../mailchimp/mailchimp.service';

@Injectable()
export class AuthService implements OnApplicationBootstrap {
  private cache: LRUCache<unknown, unknown> | undefined;

  constructor(
    private readonly jwtService: JwtService,
    private readonly userRepository: UserRepository,
    private readonly identityRepository: IdentityRepository,
    private readonly profileRepository: ProfileRepository,
    private readonly registrationTokenRepository: RegistrationTokenRepository,
    private readonly emailService: EmailService,
    private readonly refreshTokenRepository: RefreshTokenRepository,
    private readonly resetPasswordTokenRepository: ResetPasswordTokenRepository,
    private readonly dataSource: DataSource,
    private readonly projectService: ProjectService,
    private readonly cmsService: CmsService,
    private readonly ecomailService: EcomailService,
    private readonly mailchimpService: MailchimpService
  ) {}

  onApplicationBootstrap(): void {
    this.cache = new LRUCache();
  }

  async getUser(userId: string): Promise<UserEntity> {
    const user = await this.userRepository.getUserById(userId);
    if (!user) {
      throw new BadRequestException(`User ID ${userId} does not exist!`);
    }
    return user;
  }

  async getProfile(userId: string): Promise<ProfileEntity> {
    const profile = await this.profileRepository.getProfileByUserId(userId);
    if (!profile) {
      throw new BadRequestException(`Profile of User ID ${userId} does not exist!`);
    }
    return profile;
  }

  async getOrSetAuthCache(userId: string): Promise<UserEntity> {
    if (!this.cache) {
      throw new InternalServerErrorException(`Cache is undefined!`);
    }
    let user = this.cache.get(userId) as UserEntity | undefined;
    if (!user) {
      // @To-Do: add a check for blacklisted ATs
      // get user to reset cache
      user = await this.userRepository.getUserById(userId);
      if (!user) {
        throw new UnauthorizedException('User does not exist!');
      }
      this.cache.set(userId, user, 1000 * 60);
    }
    return user;
  }

  async getCredentials(identity: IdentityEntity): Promise<{ accessToken: string; refreshToken: string }> {
    const access = await this.generateToken(TokenType.ACCESS, identity.userId, identity.provider);
    const refresh = await this.generateToken(TokenType.REFRESH, identity.userId, identity.provider);
    return { accessToken: access.token, refreshToken: refresh.token };
  }

  async getIdentityOnCallback(
    firstName: string,
    lastName: string,
    email: string,
    profilePicture: string | undefined,
    provider: AuthProvider,
    subdomain?: string
  ): Promise<IdentityEntity> {
    try {
      const user = await this.userRepository.authenticateUser(email, null, firstName, lastName, profilePicture, provider, subdomain);
      return this.identityRepository.authenticateIdentity(user, provider);
    } catch (e) {
      throw new InternalServerErrorException(`Unable to fetch Identity for authentication on: ${e}`);
    }
  }

  async createUserProfile(
    userId: string,
    region: string,
    firstName: string,
    lastName: string,
    skills?: string[],
    profilePicture?: string
  ): Promise<ProfileEntity> {
    try {
      const user = await this.userRepository.updateUserById(userId, firstName, lastName, profilePicture);

      if (user.subdomain === 'innovateslovakia' && configService.isProduction()) {
        await this.mailchimpService.addUserToMailingList(user.email, firstName, lastName);
      }

      return await this.profileRepository.createProfile(user, region, skills);
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  private async generateToken(type: TokenType, userId: string, provider: AuthProvider): Promise<{ token: string; tokenId: string }> {
    const payload = { userId, type, provider };
    const jwtid = uuid.v4();
    const token = await this.jwtService.signAsync(payload, {
      expiresIn:
        type === TokenType.REFRESH ? Number(configService.get(ConfigKeys.JWT_REFRESH_TKN_AGE, '864000000')) : 30 * 24 * 60 * 60 * 1000,
      jwtid,
    });

    if (type === TokenType.REFRESH && provider === AuthProvider.EMAIL) {
      const user = await this.userRepository.getUserById(userId);
      await this.refreshTokenRepository.saveToken(user, provider, jwtid);
    }

    return { token, tokenId: jwtid };
  }

  async register(
    data: {
      email: string;
      password: string;
    },
    domain: string
  ): Promise<{ token: RegistrationTokenEntity; accessToken?: string; refreshToken?: string; user?: UserEntity }> {
    const existingUser = await this.userRepository.getUserByEmail(data.email);
    const subdomain = getSubdomainFromUrl(domain);
    if (existingUser) {
      throw new UserAlreadyExists();
    }

    const jwtPayload = {
      email: data.email,
    };

    const verificationToken = await this.jwtService.signAsync(jwtPayload);
    const token = await this.registrationTokenRepository.createRegistrationToken(data, verificationToken);
    const hashedPassword = await hashPassword(data.password);
    const user = await this.userRepository.authenticateUser(data.email, hashedPassword, null, null, null, null, subdomain);
    const identity = await this.identityRepository.authenticateIdentity(user, AuthProvider.EMAIL);
    await this.emailService.sendVerificationEmail(token, domain);

    if (user.subdomain === 'default') {
      await this.ecomailService.addUserToMailingList(data.email);
    }

    const { accessToken, refreshToken } = await this.getCredentials(identity);

    return { token, accessToken, refreshToken, user };
  }

  async verifyUser(data: VerifyUserInputType): Promise<{ accessToken?: string; refreshToken?: string; user?: UserEntity }> {
    let token: RegistrationTokenEntity;

    try {
      token = await this.registrationTokenRepository.findRegistrationToken(data);
    } catch (e) {
      throw new InvalidVerificationToken();
    }

    // Removed for now, will be validated later
    // if (token.validUntil < new Date()) {
    //   throw new InvalidVerificationToken('Verification token expired');
    // }

    const user = await this.userRepository.authenticateUser(token.email, token.password);

    if (token.used && user.emailVerification !== EmailVerification.DONE) {
      throw new InvalidVerificationToken();
    }

    if (user.emailVerification !== EmailVerification.DONE) {
      user.emailVerification = EmailVerification.DONE;
      await this.userRepository.save(user);

      token.used = true;
      await this.registrationTokenRepository.save(token);
    }

    const identity = await this.identityRepository.authenticateIdentity(user, AuthProvider.EMAIL);
    const { accessToken, refreshToken } = await this.getCredentials(identity);

    return {
      accessToken,
      refreshToken,
      user,
    };
  }

  async resendVerificationEmail(data: { requestId: string }, domain: string): Promise<void> {
    const token = await this.registrationTokenRepository.findOne({ where: { id: data.requestId, used: false } });
    if (token) await this.emailService.sendVerificationEmail(token, domain);
  }

  async login(data: { email: string; password: string }): Promise<{ accessToken?: string; refreshToken?: string; user?: UserEntity }> {
    const user = await this.userRepository.signIn(data);
    const identity = await this.identityRepository.getIdentityByUserId(user.id, AuthProvider.EMAIL);
    const { accessToken, refreshToken } = await this.getCredentials(identity);

    return {
      accessToken,
      refreshToken,
      user,
    };
  }

  async refreshAccessToken(
    oldRefreshToken: string,
    userId: string
  ): Promise<{ accessToken: string; refreshToken: string; user: UserEntity }> {
    let refreshTokenEntity: RefreshTokenEntity;
    const identity = await this.identityRepository.getIdentityByUserId(userId, AuthProvider.EMAIL);

    const verifiedToken = await this.jwtService.verify(oldRefreshToken);

    try {
      refreshTokenEntity = await this.refreshTokenRepository.findToken(verifiedToken.jti, false);
    } catch (e) {
      throw new BadRequestException('Invalid refresh token');
    }

    // invalidate old token
    await this.refreshTokenRepository.deleteToken(verifiedToken.jti);

    const { accessToken, refreshToken } = await this.getCredentials(identity);

    return {
      accessToken,
      refreshToken,
      user: refreshTokenEntity.user,
    };
  }

  async saveOnboardingData(user: UserEntity, data: OnboardingInput, subdomain: string): Promise<UserEntity> {
    try {
      const onboardingUserName = await this.userRepository.saveOnboardingData(user.id, data);
      await this.projectService.createProject(user, `Napad ${onboardingUserName} (přejmenuj mě)`, null, null, null, subdomain);
      const onboardedUser = await this.userRepository.getUserById(user.id);
      return onboardedUser;
    } catch (e) {
      throw new BadRequestException();
    }
  }

  async requestPasswordReset(email: string, domain: string): Promise<ResetPasswordTokenEntity | null> {
    const user = await this.userRepository.findOne({ where: { email } });
    if (!user) return;
    const token = await this.resetPasswordTokenRepository.createResetPasswordToken(user);
    await this.emailService.sendResetPasswordEmail(token, domain);
    return token;
  }

  async resetPassword(data: ResetPasswordInputType): Promise<void> {
    let token: ResetPasswordTokenEntity;
    try {
      token = await this.resetPasswordTokenRepository.findResetPasswordToken(data.code, data.id);
    } catch (e) {
      throw new BadRequestException('Invalid token');
    }

    if (token.validUntil < new Date()) throw new BadRequestException('Token expired');

    // checking here if user has identity with email provider - if not we create one
    await this.identityRepository.authenticateIdentity(token.user, AuthProvider.EMAIL);
    token.user.password = await hashPassword(data.password);
    await this.userRepository.save(token.user);
    await this.resetPasswordTokenRepository.deleteResetPasswordToken(token);
  }

  async resendResetPasswordEmail(requestId: string, domain: string): Promise<void> {
    const token = await this.resetPasswordTokenRepository.findTokenById(requestId);
    try {
      if (token) await this.emailService.sendResetPasswordEmail(token, domain);
    } catch (e) {
      throw new BadRequestException('Something went wrong');
    }
  }

  async editUserProfile(userId: string, data: EditUserProfileInput): Promise<ProfileEntity> {
    try {
      const user = await this.userRepository.getUserById(userId);
      if (data.profilePicture) {
        await this.cmsService.deleteOldImageFromCms(user.profilePicture);
      }
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
    try {
      const user = await this.userRepository.updateUserById(userId, data.firstName, data.lastName, data.profilePicture);
      return await this.profileRepository.editProfile(user.id, data.region, data.skills);
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  async deleteUserProfile(userId: string): Promise<UserEntity> {
    try {
      const identities = await this.identityRepository.getIdentitesByUserId(userId);
      const user = await this.userRepository.deleteUser(userId);
      const projects = await this.projectService.archiveAllProjects(userId);

      await this.dataSource.transaction(async (manager) => {
        await manager.remove(identities);
        await manager.save(user);
        await manager.save(projects);
      });

      return await this.userRepository.findOne({ where: { id: userId } });
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

  async editUserSubdomain(userId: string, domain: any): Promise<void> {
    try {
      const user = await this.userRepository.findOne({ where: { id: userId } });
      const subdomain = getSubdomainFromUrl(domain);
      user.subdomain = subdomain;
      await this.userRepository.save(user);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }
}
