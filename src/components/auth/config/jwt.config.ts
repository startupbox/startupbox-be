import { JwtModuleOptions, JwtSecretRequestType } from '@nestjs/jwt';
import { TokenType } from '../interfaces/tokenType.enum';
import { configService } from '../../../config/config.service';
import { ConfigKeys } from '../../../config/configKeys.enum';
import { TokenPayload } from '../interfaces/tokenPayload.interface';

export const jwtConfig: JwtModuleOptions = {
  secretOrKeyProvider: (requestType: JwtSecretRequestType, tokenOrPayload: unknown) => {
    const tokenType = (<TokenPayload>tokenOrPayload).type;
    switch (requestType) {
      case JwtSecretRequestType.SIGN:
        if (tokenType === TokenType.REFRESH) {
          return configService.get(ConfigKeys.JWT_REFRESH_PRIVATE_KEY);
        } else if (tokenType === TokenType.ACCESS) {
          return configService.get(ConfigKeys.JWT_ACCESS_PRIVATE_KEY);
        } else return configService.get(ConfigKeys.JWT_ACCESS_PRIVATE_KEY);
      case JwtSecretRequestType.VERIFY:
        return configService.get(ConfigKeys.JWT_REFRESH_PUBLIC_KEY);
      default:
        return 'very very hard to guess passphrase';
    }
  },
  signOptions: {
    algorithm: 'RS256',
  },
  verifyOptions: {
    algorithms: ['RS256'],
  },
};
