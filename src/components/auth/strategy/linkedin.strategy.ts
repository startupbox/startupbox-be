import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, StrategyOption, Profile } from 'passport-linkedin-oauth2';
import { configService } from '../../../config/config.service';
import { ConfigKeys } from '../../../config/configKeys.enum';
import { getServerBaseUri } from '../utils/uri.utils';
import { AuthService } from '../auth.service';
import { AuthProvider } from '../interfaces/authProvider.enum';
import { IdentityEntity } from '../entity/identity.entity';
import { Request } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
@Injectable()
export class LinkedInStrategy extends PassportStrategy(Strategy, 'linkedin') {
  constructor(private readonly authService: AuthService) {
    super(<StrategyOption>{
      clientID: configService.get(ConfigKeys.CLIENT_ID_LINKEDIN),
      clientSecret: configService.get(ConfigKeys.CLIENT_SECRET_LINKEDIN),
      callbackURL: `${getServerBaseUri()}/api/v1/auth/linkedin/callback`,
      scope: ['r_emailaddress', 'r_liteprofile'],
      passReqToCallback: true,
    });
  }

  authenticate(req: Request<ParamsDictionary, any, any, ParsedQs>, options?: any): void {
    options.state = req.query.domain;
    super.authenticate(req, options);
  }

  async validate(
    request: Request,
    _accessToken: string,
    _refreshToken: string,
    profile: IProfile,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    done: (err: any, result: IdentityEntity | boolean) => void
  ): Promise<void> {
    try {
      const identity = await this.authService.getIdentityOnCallback(
        profile.name.givenName,
        profile.name.familyName,
        profile.emails[0].value,
        profile.photos.length !== 0 ? (profile.photos.length > 1 ? profile.photos[1].value : profile.photos.pop().value) : undefined,
        AuthProvider.LINKEDIN
      );
      done(null, identity);
    } catch (error) {
      done(error, false);
    }
  }
}

interface IProfile extends Omit<Profile, 'photos'> {
  photos?: { value: string }[];
}
