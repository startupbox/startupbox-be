import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { passportConfig } from '../config/passport.config';
import { AuthService } from '../auth.service';
import { FacebookStrategy } from './facebook.strategy';

const mockAuthService = () => ({
  getOrSetAuthCache: jest.fn(),
});

describe('FacebookStrategy', () => {
  let strategy: FacebookStrategy;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let service: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PassportModule.register(passportConfig)],
      providers: [
        FacebookStrategy,
        {
          provide: AuthService,
          useFactory: mockAuthService,
        },
      ],
    }).compile();

    strategy = module.get<FacebookStrategy>(FacebookStrategy);
    service = module.get<AuthService>(AuthService);
  });

  it('strategy should be defined', () => {
    expect(strategy).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });
});
