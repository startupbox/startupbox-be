import { Test, TestingModule } from '@nestjs/testing';
import { LinkedInStrategy } from './linkedin.strategy';
import { PassportModule } from '@nestjs/passport';
import { passportConfig } from '../config/passport.config';
import { AuthService } from '../auth.service';

const mockAuthService = () => ({
  getOrSetAuthCache: jest.fn(),
});

describe('LinkedInStrategy', () => {
  let strategy: LinkedInStrategy;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let service: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PassportModule.register(passportConfig)],
      providers: [
        LinkedInStrategy,
        {
          provide: AuthService,
          useFactory: mockAuthService,
        },
      ],
    }).compile();

    strategy = module.get<LinkedInStrategy>(LinkedInStrategy);
    service = module.get<AuthService>(AuthService);
  });

  it('strategy should be defined', () => {
    expect(strategy).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });
});
