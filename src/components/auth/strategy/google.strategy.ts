import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, Profile } from 'passport-google-oauth20';
import { configService } from '../../../config/config.service';
import { ConfigKeys } from '../../../config/configKeys.enum';
import { getServerBaseUri } from '../utils/uri.utils';
import { AuthService } from '../auth.service';
import { AuthProvider } from '../interfaces/authProvider.enum';
import { IdentityEntity } from '../entity/identity.entity';
import { Request } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private readonly authService: AuthService) {
    super(<any>{
      clientID: configService.get(ConfigKeys.CLIENT_ID_GOOGLE),
      clientSecret: configService.get(ConfigKeys.CLIENT_SECRET_GOOGLE),
      callbackURL: `${getServerBaseUri()}/api/v1/auth/google/callback`,
      scope: ['email', 'profile'],
      passReqToCallback: true,
    });
  }

  authenticate(req: Request<ParamsDictionary, any, any, ParsedQs>, options?: any): void {
    options.state = req.query.domain;
    super.authenticate(req, options);
  }

  async validate(
    request: Request,
    _accessToken: string,
    _refreshToken: string,
    profile: IProfile,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    done: (err: any, result: IdentityEntity | boolean) => void
  ): Promise<void> {
    try {
      const identity = await this.authService.getIdentityOnCallback(
        profile.name.givenName,
        profile.name.familyName,
        profile.emails[0].value,
        profile.photos.length !== 0 ? (profile.photos.length > 1 ? profile.photos[1].value : profile.photos.pop().value) : undefined,
        AuthProvider.GOOGLE
      );
      done(null, identity);
    } catch (error) {
      done(error, false);
    }
  }
}

interface IProfile extends Omit<Profile, 'photos'> {
  photos?: { value: string }[];
}
