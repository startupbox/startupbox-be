import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { configService } from '../../../config/config.service';
import { ConfigKeys } from '../../../config/configKeys.enum';
import { UserEntity } from '../entity/user.entity';
import { TokenPayload } from '../interfaces/tokenPayload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get(ConfigKeys.JWT_ACCESS_PUBLIC_KEY),
      algorithm: ['RS256'],
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async validate(jwt: TokenPayload, done: (err: any, result: UserEntity | boolean) => void): Promise<void> {
    try {
      // @To-Do: Pass in jwt.id as the second parameter to check if the token is blacklisted
      const user = await this.authService.getOrSetAuthCache(jwt.userId);
      done(null, user);
    } catch (error) {
      done(error, false);
    }
  }
}
