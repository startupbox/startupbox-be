import { INestApplication } from '@nestjs/common';
import { ProfileRepository } from './profile.repository';
import { TestingModule, Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../entity/user.entity';
import { UserRepository } from './user.repository';
import { userFactory } from '../../../../test/factories/user.factory';
import { ConfigService } from '../../../config/config.service';
import dataSource from '../../../data.source';
import { ProfileEntity } from '../entity/profile.entity';

describe('ProfileRepository', () => {
  let app: INestApplication;
  let profileRepository: ProfileRepository;
  let userRepository: UserRepository;
  let mockInsertedUser: UserEntity;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({ useClass: ConfigService }), TypeOrmModule.forFeature([ProfileEntity, UserEntity])],
      providers: [UserRepository, ProfileRepository],
    }).compile();

    app = module.createNestApplication();
    await app.init();
    await dataSource.initialize();

    profileRepository = module.get<ProfileRepository>(ProfileRepository);
    userRepository = module.get<UserRepository>(UserRepository);

    const user = userFactory();
    mockInsertedUser = await userRepository.createUser(user.firstName, user.lastName, user.email, user.profilePicture);
    expect(await userRepository.count()).toEqual(1);
  });

  // use this afterEach only for repository tests
  afterEach(async () => {
    await dataSource.dropDatabase();
    await dataSource.destroy();
    await app.close();
  });

  it('profileRepository should be defined', () => {
    expect(profileRepository).toBeDefined();
  });

  it('userRepository should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  it('mockInsertedUser should be defined', () => {
    expect(mockInsertedUser).toBeDefined();
  });
});
