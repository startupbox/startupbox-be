import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { IdentityEntity } from '../entity/identity.entity';
import { UserEntity } from '../entity/user.entity';
import { AuthProvider } from '../interfaces/authProvider.enum';

@Injectable()
export class IdentityRepository extends Repository<IdentityEntity> {
  constructor(private dataSource: DataSource) {
    super(IdentityEntity, dataSource.createEntityManager());
  }

  async getIdentityByUserId(userId: string, provider: AuthProvider): Promise<IdentityEntity> {
    return this.findOne({ where: { userId, provider } });
  }

  async createIdentity(user: UserEntity, provider: AuthProvider): Promise<IdentityEntity> {
    const i = new IdentityEntity();
    i.user = user;
    i.provider = provider;
    return this.save(i);
  }

  async authenticateIdentity(user: UserEntity, provider: AuthProvider): Promise<IdentityEntity> {
    const identity = await this.getIdentityByUserId(user.id, provider);
    return identity ? identity : await this.createIdentity(user, provider);
  }

  async getIdentitesByUserId(userId: string): Promise<IdentityEntity[]> {
    return this.find({ where: { userId } });
  }
}
