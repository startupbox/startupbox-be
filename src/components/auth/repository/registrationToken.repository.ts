import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { RegistrationTokenEntity } from '../entity/registrationToken.entity';
import { hashPassword } from '../utils/hashPassword';

@Injectable()
export class RegistrationTokenRepository extends Repository<RegistrationTokenEntity> {
  constructor(private dataSource: DataSource) {
    super(RegistrationTokenEntity, dataSource.createEntityManager());
  }

  createRegistrationToken = async (
    data: { password: string; email: string },
    verificationToken: string
  ): Promise<RegistrationTokenEntity> => {
    const token = new RegistrationTokenEntity();
    token.verificationToken = verificationToken;
    token.password = await hashPassword(data.password);
    token.email = data.email;

    return await this.save(token);
  };

  findRegistrationToken = async (data: { requestId: string; verificationToken: string }): Promise<RegistrationTokenEntity> | null => {
    const token = await this.findOneOrFail({
      where: {
        id: data.requestId,
        verificationToken: data.verificationToken,
      },
    });
    return token;
  };
}
