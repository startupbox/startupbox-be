import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from './user.repository';
import { INestApplication } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '../../../config/config.service';
import { UserRole } from '../interfaces/userRole.enum';
import { EmailVerification } from '../interfaces/emailVerification.enum';
import dataSource from '../../../data.source';
import { UserEntity } from '../entity/user.entity';

describe('UserRepository', () => {
  let app: INestApplication;
  let repository: UserRepository;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({ useClass: ConfigService }), TypeOrmModule.forFeature([UserEntity])],
      providers: [UserRepository],
    }).compile();

    app = module.createNestApplication();
    await app.init();
    await dataSource.initialize();

    repository = module.get<UserRepository>(UserRepository);
  });

  // use this afterEach only for repository tests
  afterEach(async () => {
    await dataSource.dropDatabase();
    await dataSource.destroy();
    await app.close();
  });

  it('repository should be defined', () => {
    expect(repository).toBeDefined();
  });

  it('should insert a user to database', async () => {
    expect(await repository.count()).toEqual(0);
    await repository.insert({
      firstName: 'Bob',
      lastName: 'The Builder',
      email: 'asdf@example.com',
      profilePicture: 'http://cdn.profile-pictures.com/user/asdfghjkl/100_100',
    });
    expect(await repository.count()).toEqual(1);
  });

  it('should create a user and save it to database', async () => {
    expect(await repository.count()).toEqual(0);
    const result = await repository.createUser(
      'email@email.com',
      null,
      'firstName',
      'lastName',
      'http://cdn.profile-pictures.com/user/asdfghjkl/100_100'
    );
    expect(await repository.count()).toEqual(1);
    expect(await repository.findOne({ where: { email: 'email@email.com' } })).toEqual({
      id: result.id,
      createdAt: result.createdAt,
      updatedAt: result.updatedAt,
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'email@email.com',
      role: UserRole.CLIENT,
      profilePicture: 'http://cdn.profile-pictures.com/user/asdfghjkl/100_100',
      password: null,
      emailVerification: EmailVerification.PENDING,
      onboardingData: null,
      agreedToTermsAndConditions: true,
      onboardingResult: null,
      archived: false,
      subdomain: 'default',
    });
  });

  it('should get a user by email', async () => {
    expect(await repository.count()).toEqual(0);
    const insertedUser = await repository.createUser(
      'email@email.com',
      null,
      'firstName',
      'lastName',
      'http://cdn.profile-pictures.com/user/asdfghjkl/100_100'
    );
    const result = await repository.getUserByEmail('email@email.com');
    expect(await repository.count()).toEqual(1);
    expect(result).toEqual({
      id: insertedUser.id,
      createdAt: insertedUser.createdAt,
      updatedAt: insertedUser.updatedAt,
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'email@email.com',
      role: UserRole.CLIENT,
      profilePicture: 'http://cdn.profile-pictures.com/user/asdfghjkl/100_100',
      password: null,
      emailVerification: EmailVerification.PENDING,
      onboardingData: null,
      agreedToTermsAndConditions: true,
      onboardingResult: null,
      archived: false,
      subdomain: 'default',
    });
  });

  it('should get a user by ID', async () => {
    expect(await repository.count()).toEqual(0);
    const insertedUser = await repository.createUser(
      'email@email.com',
      null,
      'firstName',
      'lastName',
      'http://cdn.profile-pictures.com/user/asdfghjkl/100_100'
    );
    const result = await repository.getUserById(insertedUser.id);
    expect(await repository.count()).toEqual(1);
    expect(result).toEqual({
      id: insertedUser.id,
      createdAt: insertedUser.createdAt,
      updatedAt: insertedUser.updatedAt,
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'email@email.com',
      role: UserRole.CLIENT,
      profilePicture: 'http://cdn.profile-pictures.com/user/asdfghjkl/100_100',
      profile: null,
      identities: [],
      projects: [],
      password: null,
      emailVerification: EmailVerification.PENDING,
      onboardingData: null,
      agreedToTermsAndConditions: true,
      onboardingResult: null,
      archived: false,
      subdomain: 'default',
    });
  });
});
