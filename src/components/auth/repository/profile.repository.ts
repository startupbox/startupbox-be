import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProfileEntity } from '../entity/profile.entity';
import { UserEntity } from '../entity/user.entity';

@Injectable()
export class ProfileRepository extends Repository<ProfileEntity> {
  constructor(private dataSource: DataSource) {
    super(ProfileEntity, dataSource.createEntityManager());
  }

  async createProfile(user: UserEntity, region: string, skills?: string[]): Promise<ProfileEntity> {
    const p = new ProfileEntity();
    p.region = region;
    p.skills = skills;
    p.user = user;
    return this.save(p);
  }

  async getProfileByUserId(userId: string): Promise<ProfileEntity> {
    const profile = await this.createQueryBuilder('profile')
      .where('profile.userId = :userId', { userId })
      .leftJoinAndSelect('profile.user', 'user')
      .leftJoinAndSelect('user.projects', 'projects')
      .leftJoinAndSelect('user.profile', 'userProfile')
      .getOne();
    return profile;
  }

  async editProfile(userId: string, region?: string, skills?: string[]): Promise<ProfileEntity> {
    const profile = await this.getProfileByUserId(userId);
    region && (profile.region = region);
    skills && (profile.skills = skills);
    await this.save(profile);
    return profile;
  }
}
