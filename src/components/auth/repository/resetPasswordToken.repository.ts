import moment from 'moment';
import randomstring from 'randomstring';
import { DataSource, Repository } from 'typeorm';
import { ResetPasswordTokenEntity } from '../entity/resetPasswordToken.entity';
import { UserEntity } from '../entity/user.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ResetPasswordTokenRepository extends Repository<ResetPasswordTokenEntity> {
  constructor(private dataSource: DataSource) {
    super(ResetPasswordTokenEntity, dataSource.createEntityManager());
  }

  createResetPasswordToken = async (user: UserEntity): Promise<ResetPasswordTokenEntity> => {
    const token = new ResetPasswordTokenEntity();
    token.code = randomstring.generate({ length: 6, charset: 'numeric' });
    token.user = user;
    token.validUntil = moment().add(1, 'days').toDate();

    return await this.save(token);
  };

  findResetPasswordToken = async (code: string, id: string): Promise<ResetPasswordTokenEntity> | null => {
    const token = await this.findOneOrFail({
      where: {
        code,
        id,
      },
      relations: ['user'],
    });
    return token;
  };

  findTokenById = async (id: string): Promise<ResetPasswordTokenEntity> | null => {
    return await this.findOne({ where: { id } });
  };

  deleteResetPasswordToken = async (token: ResetPasswordTokenEntity): Promise<ResetPasswordTokenEntity> => {
    return await this.remove(token);
  };
}
