import { INestApplication } from '@nestjs/common';
import { IdentityRepository } from './identity.repository';
import { TestingModule, Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UserEntity } from '../entity/user.entity';
import { userFactory } from '../../../../test/factories/user.factory';
import { AuthProvider } from '../interfaces/authProvider.enum';
import { ConfigService } from '../../../config/config.service';
import dataSource from '../../../data.source';
import { IdentityEntity } from '../entity/identity.entity';

describe('IdentityRepository', () => {
  let app: INestApplication;
  let identityRepository: IdentityRepository;
  let userRepository: UserRepository;
  let mockInsertedUser: UserEntity;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({ useClass: ConfigService }), TypeOrmModule.forFeature([IdentityEntity, UserEntity])],
      providers: [UserRepository, IdentityRepository],
    }).compile();

    app = module.createNestApplication();
    await dataSource.initialize();
    await app.init();

    identityRepository = module.get<IdentityRepository>(IdentityRepository);
    userRepository = module.get<UserRepository>(UserRepository);

    const user = userFactory();
    mockInsertedUser = await userRepository.createUser(user.firstName, user.lastName, user.email, user.profilePicture);
    expect(await userRepository.count()).toEqual(1);
  });

  // use this afterEach only for repository tests
  afterEach(async () => {
    await dataSource.dropDatabase();
    await dataSource.destroy();
    await app.close();
  });

  it('identityRepository should be defined', () => {
    expect(identityRepository).toBeDefined();
  });

  it('userRepository should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  it('mockInsertedUser should be defined', () => {
    expect(mockInsertedUser).toBeDefined();
  });

  it('should create an identity and save it to database', async () => {
    expect(await identityRepository.count()).toEqual(0);
    const result = await identityRepository.createIdentity(mockInsertedUser, AuthProvider.LINKEDIN);

    expect(await identityRepository.count()).toEqual(1);
    expect(await identityRepository.findOne({ where: { userId: mockInsertedUser.id } })).toEqual({
      id: result.id,
      createdAt: result.createdAt,
      updatedAt: result.updatedAt,
      provider: AuthProvider.LINKEDIN,
      userId: mockInsertedUser.id,
    });
  });

  it('should get identity by user ID', async () => {
    expect(await identityRepository.count()).toEqual(0);
    const insertedIdentity = await identityRepository.createIdentity(mockInsertedUser, AuthProvider.LINKEDIN);
    const result = await identityRepository.getIdentityByUserId(mockInsertedUser.id, insertedIdentity.provider);

    expect(await identityRepository.count()).toEqual(1);
    expect(result).toEqual({
      id: insertedIdentity.id,
      createdAt: insertedIdentity.createdAt,
      updatedAt: insertedIdentity.updatedAt,
      provider: insertedIdentity.provider,
      userId: mockInsertedUser.id,
    });
  });

  it('should GET identity if it exists', async () => {
    expect(await identityRepository.count()).toEqual(0);
    const insertedIdentity = await identityRepository.createIdentity(mockInsertedUser, AuthProvider.LINKEDIN);
    expect(await identityRepository.count()).toEqual(1);
    const spyGetIdentityByUserId = jest.spyOn(identityRepository, 'getIdentityByUserId');
    const spyCreateIdentity = jest.spyOn(identityRepository, 'createIdentity');
    const result = await identityRepository.authenticateIdentity(mockInsertedUser, AuthProvider.LINKEDIN);

    expect(spyGetIdentityByUserId).toHaveBeenCalledTimes(1);
    expect(spyCreateIdentity).toHaveBeenCalledTimes(0);
    expect(result).toEqual({
      id: insertedIdentity.id,
      createdAt: insertedIdentity.createdAt,
      updatedAt: insertedIdentity.updatedAt,
      provider: insertedIdentity.provider,
      userId: mockInsertedUser.id,
    });
  });

  it('should CREATE identity if it does not exist', async () => {
    expect(await identityRepository.count()).toEqual(0);
    const spyGetIdentityByUserId = jest.spyOn(identityRepository, 'getIdentityByUserId');
    const spyCreateIdentity = jest.spyOn(identityRepository, 'createIdentity');
    const result = await identityRepository.authenticateIdentity(mockInsertedUser, AuthProvider.LINKEDIN);

    expect(spyGetIdentityByUserId).toHaveBeenCalledTimes(1);
    expect(spyCreateIdentity).toHaveBeenCalledTimes(1);
    expect(result).toEqual({
      id: result.id,
      createdAt: result.createdAt,
      updatedAt: result.updatedAt,
      provider: AuthProvider.LINKEDIN,
      userId: mockInsertedUser.id,
      user: mockInsertedUser,
    });
  });
});
