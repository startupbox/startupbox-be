import { DataSource, Repository } from 'typeorm';
import { UserEntity } from '../entity/user.entity';
import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AuthProvider } from '../interfaces/authProvider.enum';
import { EmailVerification } from '../interfaces/emailVerification.enum';
import { OnboardingInput } from '../inputs/onboarding.input';

@Injectable()
export class UserRepository extends Repository<UserEntity> {
  constructor(private dataSource: DataSource) {
    super(UserEntity, dataSource.createEntityManager());
  }

  async getUserById(id: string): Promise<UserEntity | undefined> {
    const user = await this.createQueryBuilder('user')
      .where('user.id = :id', { id })
      .leftJoinAndSelect('user.profile', 'profile')
      .leftJoinAndSelect('user.identities', 'identities')
      .leftJoinAndSelect('user.projects', 'projects')
      .getOne();
    return user;
  }

  async getUserByEmail(email: string): Promise<UserEntity | undefined> {
    return this.findOne({ where: { email } });
  }

  async createUser(
    email: string,
    password?: string,
    firstName?: string,
    lastName?: string,
    profilePicture?: string,
    provider?: AuthProvider,
    subdomain?: string
  ): Promise<UserEntity> {
    const u = new UserEntity();
    u.firstName = firstName ? firstName : undefined;
    u.lastName = lastName ? lastName : undefined;
    u.email = email;
    u.profilePicture = profilePicture ? profilePicture : undefined;
    u.password = password ? password : undefined;
    u.agreedToTermsAndConditions = true;
    u.subdomain = subdomain;

    if (provider && provider !== AuthProvider.EMAIL) {
      u.emailVerification = EmailVerification.NOT_NEEDED;
    }

    return this.save(u);
  }

  async updateUserById(id: string, firstName?: string, lastName?: string, profilePicture?: string): Promise<UserEntity> {
    const user = await this.getUserById(id);
    firstName && (user.firstName = firstName);
    lastName && (user.lastName = lastName);
    user.profilePicture = profilePicture ? profilePicture : undefined;

    try {
      await this.save(user);
    } catch (e) {
      throw new InternalServerErrorException(`Failed to update User ID: ${id}`);
    }
    return user;
  }

  async authenticateUser(
    email: string,
    password?: string,
    firstName?: string,
    lastName?: string,
    profilePicture?: string,
    provider?: AuthProvider,
    subdomain?: string
  ): Promise<UserEntity> {
    let user = await this.getUserByEmail(email);
    if (!user) {
      user = await this.createUser(email, password, firstName, lastName, profilePicture, provider, subdomain);
    }
    if (
      (firstName && user.firstName !== firstName) ||
      (lastName && user.lastName !== lastName) ||
      (profilePicture && user.profilePicture !== profilePicture)
    ) {
      user.firstName = firstName;
      user.lastName = lastName;
      user.profilePicture = profilePicture;
      await this.update({ id: user.id }, user);
    }
    return user;
  }

  async signIn(data: { email: string; password: string }): Promise<UserEntity> {
    const user = await this.findOneOrFail({
      where: {
        email: data.email,
      },
    });

    if (await bcrypt.compare(data.password, user.password)) {
      return user;
    }

    throw new BadRequestException('Wrong credentials');
  }

  async saveOnboardingData(id: string, data: OnboardingInput): Promise<string> {
    const user = await this.getUserById(id);
    user.onboardingData = data.onboardingAnswers;
    user.onboardingResult = data.result;
    try {
      await this.save(user);
    } catch (e) {
      throw new InternalServerErrorException(`Failed to save onboarding data for User ID: ${id}`);
    }

    return user.firstName;
  }

  async deleteUser(userId: string): Promise<UserEntity> {
    try {
      const user = await this.findOne({ where: { id: userId } });
      user.archived = true;
      user.email = null;
      user.firstName = null;
      user.lastName = null;
      user.password = null;
      user.profilePicture = null;
      user.comments = null;
      return user;
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

  async getAllUsers(subdomain: string): Promise<UserEntity[]> {
    return await this.createQueryBuilder('user')
      .leftJoinAndSelect('user.profile', 'profile')
      .leftJoinAndSelect('user.activityLog', 'activities')
      .where({ subdomain })
      .getMany();
  }

  async getUsersActivity(subdomain: string): Promise<Record<string, { year: number; month: number; count: number }[]>> {
    const rows = await this.createQueryBuilder('user')
      .leftJoin('user.activityLog', 'activity', 'activity.userId = user.id')
      .select([
        'user.id AS user_id',
        'EXTRACT(YEAR FROM activity.createdAt) AS year',
        'EXTRACT(MONTH FROM activity.createdAt) AS month',
        'COUNT(*) AS row_count',
      ])
      .where('user.subdomain = :subdomain', { subdomain })
      .groupBy('user.id, activity.userId, year, month')
      .orderBy('user.id, activity.userId, year, month')
      .getRawMany();

    return rows.reduce((obj, row) => {
      if (!row.year) {
        return obj;
      }
      if (!obj[row.user_id]) {
        obj[row.user_id] = [];
      }

      obj[row.user_id].push({
        year: row.year,
        month: row.month,
        count: row.row_count,
      });

      return obj;
    }, {});
  }
}
