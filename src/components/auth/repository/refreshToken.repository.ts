import { Injectable } from '@nestjs/common';
import { DataSource, DeleteResult, Repository } from 'typeorm';
import { RefreshTokenEntity } from '../entity/refreshToken.entity';
import { UserEntity } from '../entity/user.entity';
import { AuthProvider } from '../interfaces/authProvider.enum';

@Injectable()
export class RefreshTokenRepository extends Repository<RefreshTokenEntity> {
  constructor(private dataSource: DataSource) {
    super(RefreshTokenEntity, dataSource.createEntityManager());
  }

  saveToken = async (user: UserEntity, provider: AuthProvider, jwtid: string): Promise<RefreshTokenEntity> => {
    const refreshTokenEntity = new RefreshTokenEntity();
    refreshTokenEntity.id = jwtid;
    refreshTokenEntity.user = user;
    refreshTokenEntity.provider = provider;
    return await this.save(refreshTokenEntity);
  };

  deleteToken = async (id: string): Promise<DeleteResult> => {
    return await this.delete({ id });
  };

  findToken = async (id: string, revoked: boolean): Promise<RefreshTokenEntity> => {
    return await this.findOneOrFail({
      where: {
        id,
        revoked: revoked,
      },
    });
  };
}
