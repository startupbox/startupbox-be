import { InternalServerErrorException } from '@nestjs/common';
import { configService } from '../../../config/config.service';
import { ConfigKeys } from '../../../config/configKeys.enum';
import { getBaseUrl } from './domain.util';

export const getServerBaseUri = (isFrontend = false, domain?: string): string => {
  switch (configService.get(ConfigKeys.NODE_ENV)) {
    case 'test':
    case 'development':
      return isFrontend
        ? `${domain ? getBaseUrl(domain) : `http://localhost:${configService.get(ConfigKeys.FE_PORT, '3001')}`}`
        : `${configService.get(ConfigKeys.BE_BASE_URL, `http://localhost:${configService.get(ConfigKeys.PORT, '3000')}`)}`;
    case 'staging':
      return isFrontend
        ? `${domain ? getBaseUrl(domain) : 'https://staging.startupbox.app'}`
        : `${configService.get(ConfigKeys.BE_BASE_URL, 'https://startupbox-staging.herokuapp.com')}`;
    case 'production':
      return isFrontend
        ? `${domain ? getBaseUrl(domain) : 'https://startupbox.cz'}`
        : `${configService.get(ConfigKeys.BE_BASE_URL, 'https://startupbox.cz')}`;
    default:
      throw new InternalServerErrorException(`Server mode ${configService.get(ConfigKeys.NODE_ENV)} is not supported`);
  }
};

export const getCookieBaseUri = (): string => {
  switch (configService.get(ConfigKeys.NODE_ENV)) {
    case 'test':
    case 'development':
      return '';
    case 'production':
      return `${configService.get(ConfigKeys.FE_BASE_URL, 'startupbox.cz')}`;
    default:
      throw new InternalServerErrorException(`Server mode ${configService.get(ConfigKeys.NODE_ENV)} is not supported`);
  }
};
