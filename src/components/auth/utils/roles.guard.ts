import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Reflector } from '@nestjs/core';
import { UserEntity } from '../entity/user.entity';
import { UserRole } from '../interfaces/userRole.enum';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const allowedRoles = this.reflector.get<UserRole[]>('allowedRoles', context.getHandler());
    if (!allowedRoles) {
      return true;
    }
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    const user: UserEntity = req.user;
    return allowedRoles.includes(user.role);
  }
}
