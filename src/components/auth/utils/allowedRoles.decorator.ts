import { SetMetadata, CustomDecorator } from '@nestjs/common';
import { UserRole } from '../interfaces/userRole.enum';

export const AllowedRoles = (...userRoles: UserRole[]): CustomDecorator<string> => SetMetadata('allowedRoles', userRoles);
