import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { UserEntity } from '../entity/user.entity';

export const GetUser = createParamDecorator((data: unknown, context: ExecutionContext): UserEntity => {
  const ctx = GqlExecutionContext.create(context);
  return ctx.getContext().req.user;
});
