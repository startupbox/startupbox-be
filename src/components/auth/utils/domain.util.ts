// TODO: extend for all cases where there are 2 subdomains
export const getSubdomainFromUrl = (url: string): string => {
  const domain = /:\/\/([^/]+)/.exec(url)[1];
  const split = domain.split('.');
  switch (split[0]) {
    case 'startupbox-staging':
    case 'staging':
    case 'dev':
      return 'default';
    default:
      break;
  }
  return split.length > 2 ? split[0] : 'default';
};

export const getBaseUrl = (url: string): string => {
  const urlArray = url.split('/');
  const protocol = urlArray[0];
  const host = urlArray[2];
  return protocol + '//' + host;
};
