import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { UserType } from './user.type';

@ObjectType('Profile')
export class ProfileType extends BasicType {
  @Field({ nullable: true })
  region?: string;

  @Field(() => [String], { nullable: true })
  skills?: string[];

  @Field(() => UserType, { nullable: true })
  user?: UserType;
}
