import { Field, ObjectType } from '@nestjs/graphql';
import { BasicResponseType } from '../../../utils/basicResponseType';
import { UserType } from './user.type';

@ObjectType()
export class RegistrationResponseType extends BasicResponseType {
  @Field({ nullable: true })
  requestId?: string;

  @Field({ nullable: true })
  verificationToken?: string;

  @Field({ nullable: true })
  accessToken?: string;

  @Field({ nullable: true })
  refreshToken?: string;

  @Field({ nullable: true })
  user?: UserType;

  @Field({ nullable: true })
  expiresIn?: number;

  @Field({ nullable: true })
  refreshExpiresIn?: number;
}
