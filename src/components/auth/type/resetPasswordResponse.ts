import { ObjectType, Field } from '@nestjs/graphql';
import { BasicResponseType } from '../../../utils/basicResponseType';

@ObjectType()
export class ResetPasswordMutationResponseType extends BasicResponseType {
  @Field({ nullable: true })
  requestId?: string;

  @Field({ nullable: true })
  verificationCode?: string;
}
