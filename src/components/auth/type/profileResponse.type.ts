import { ObjectType, Field } from '@nestjs/graphql';
import { BasicResponseType } from '../../../utils/basicResponseType';
import { ProfileEntity } from '../entity/profile.entity';
import { ProfileType } from './profile.type';

@ObjectType()
export class ProfileResponseType extends BasicResponseType {
  @Field(() => ProfileType)
  profile?: ProfileEntity;
}
