import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { IdentityType } from './identity.type';
import { ProfileType } from './profile.type';
import { ProjectType } from '../../../components/project/type/project.type';
import { UserRole } from '../interfaces/userRole.enum';
import { OnboardingType } from './onboardingData.type';
import { EmailVerification } from '../interfaces/emailVerification.enum';

@ObjectType('User')
export class UserType extends BasicType {
  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  profilePicture?: string;

  @Field(() => UserRole)
  role: UserRole;

  @Field(() => [IdentityType], { nullable: true })
  identities?: IdentityType[];

  @Field(() => ProfileType, { nullable: true })
  profile?: ProfileType;

  @Field(() => [ProjectType], { nullable: true })
  projects?: ProjectType[];

  @Field(() => EmailVerification)
  emailVerification?: EmailVerification;

  @Field(() => [OnboardingType], { nullable: true })
  onboardingData?: OnboardingType[];

  @Field()
  agreedToTermsAndConditions: boolean;

  @Field({ nullable: true })
  onboardingResult?: string;

  @Field()
  archived: boolean;

  @Field()
  subdomain: string;
}
