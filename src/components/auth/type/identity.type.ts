import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { AuthProvider } from '../interfaces/authProvider.enum';

@ObjectType('Identity')
export class IdentityType extends BasicType {
  @Field()
  userId: string;

  @Field(() => AuthProvider)
  provider: AuthProvider;
}
