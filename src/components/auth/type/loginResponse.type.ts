import { ObjectType, Field } from '@nestjs/graphql';
import { BasicResponseType } from '../../../utils/basicResponseType';
import { UserType } from './user.type';

@ObjectType()
export class LoginResponseType extends BasicResponseType {
  @Field({ nullable: true })
  accessToken?: string;

  @Field({ nullable: true })
  refreshToken?: string;

  @Field({ nullable: true })
  expiresIn?: number;

  @Field({ nullable: true })
  refreshExpiresIn?: number;

  @Field({ nullable: true })
  tokenType?: string;

  @Field({ nullable: true })
  user?: UserType;
}
