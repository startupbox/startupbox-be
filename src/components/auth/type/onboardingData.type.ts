import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class OnboardingType {
  @Field()
  question: string;

  @Field(() => [String])
  answer: string[];
}
