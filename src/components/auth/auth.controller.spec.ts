import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Request, Response } from 'express';
import { userFactory } from '../../../test/factories/user.factory';

const mockAuthService = () => ({
  getCredentials: jest.fn(),
  editUserSubdomain: jest.fn(),
});

describe('Auth Controller', () => {
  let controller: AuthController;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let authService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: AuthService,
          useFactory: mockAuthService,
        },
      ],
      controllers: [AuthController],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('controller should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('authService should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('linkedin', () => {
    it('endpoint for the SSO authflow should be defined', () => {
      expect(controller.getLinkedInAuth).toBeDefined();
    });

    it('endpoint for the SSO callback should be defined & it should issue credentials', async () => {
      const mockRequest = {
        user: userFactory(),
        query: {
          domain: 'http://localhost:3001',
        },
      };

      const mockResponse = () => {
        const res = <{ statusCode: number; redirect: jest.Mock; status: jest.Mock }>{};
        res.redirect = jest.fn().mockReturnValue(res);
        res.status = jest.fn(() => {
          res.statusCode = 302;
          return res;
        });
        return res;
      };

      jest
        .spyOn(authService, 'getCredentials')
        .mockImplementation(async () => ({ accessToken: 'some-token', refreshToken: 'some-other-token' }));

      await controller.callbackLinkedIn(mockRequest as unknown as Request, mockResponse() as unknown as Response);

      expect(authService.getCredentials).toHaveBeenCalledTimes(1);
      expect(authService.getCredentials).toHaveBeenCalledWith(userFactory());
    });
  });

  describe('google', () => {
    it('endpoint for the SSO authflow should be defined', () => {
      expect(controller.googleAuth).toBeDefined();
    });

    it('endpoint for the SSO callback should be defined & it should issue credentials', async () => {
      const mockRequest = {
        user: userFactory(),
        query: {
          domain: 'http://localhost:3001',
        },
      };

      const mockResponse = () => {
        const res = <{ statusCode: number; redirect: jest.Mock; status: jest.Mock }>{};
        res.redirect = jest.fn().mockReturnValue(res);
        res.status = jest.fn(() => {
          res.statusCode = 302;
          return res;
        });
        return res;
      };

      jest
        .spyOn(authService, 'getCredentials')
        .mockImplementation(async () => ({ accessToken: 'some-token', refreshToken: 'some-other-token' }));

      await controller.googleAuthCallback(mockRequest as unknown as Request, mockResponse() as unknown as Response);

      expect(authService.getCredentials).toHaveBeenCalledTimes(1);
      expect(authService.getCredentials).toHaveBeenCalledWith(userFactory());
    });
  });

  describe('facebook', () => {
    it('endpoint for the SSO authflow should be defined', () => {
      expect(controller.facebookAuth).toBeDefined();
    });

    it('endpoint for the SSO callback should be defined & it should issue credentials', async () => {
      const mockRequest = {
        user: userFactory(),
        query: {
          domain: 'http://localhost:3001',
        },
      };

      const mockResponse = () => {
        const res = <{ statusCode: number; redirect: jest.Mock; status: jest.Mock }>{};
        res.redirect = jest.fn().mockReturnValue(res);
        res.status = jest.fn(() => {
          res.statusCode = 302;
          return res;
        });
        return res;
      };

      jest
        .spyOn(authService, 'getCredentials')
        .mockImplementation(async () => ({ accessToken: 'some-token', refreshToken: 'some-other-token' }));

      await controller.facebookCallback(mockRequest as unknown as Request, mockResponse() as unknown as Response);

      expect(authService.getCredentials).toHaveBeenCalledTimes(1);
      expect(authService.getCredentials).toHaveBeenCalledWith(userFactory());
    });
  });
});
