import { Test, TestingModule } from '@nestjs/testing';
import { UnauthorizedException, InternalServerErrorException, BadRequestException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserRepository } from './repository/user.repository';
import { IdentityRepository } from './repository/identity.repository';
import { JwtService, JwtModule } from '@nestjs/jwt';
import { userFactory } from '../../../test/factories/user.factory';
import { identityFactory } from '../../../test/factories/identity.factory';
import { jwtConfig } from './config/jwt.config';
import { ProfileRepository } from './repository/profile.repository';
import { RegistrationTokenRepository } from './repository/registrationToken.repository';
import { RefreshTokenRepository } from './repository/refreshToken.repository';
import { EmailService } from '../email/email.service';
import { ResetPasswordTokenRepository } from '../auth/repository/resetPasswordToken.repository';
import { DataSource } from 'typeorm';
import { ProjectService } from '../../components/project/project.service';
import { CmsService } from '../cms/cms.service';
import { EcomailService } from '../ecomail/ecomail.service';
import { MailchimpService } from '../mailchimp/mailchimp.service';

const mockUserRepository = () => ({
  getUserById: jest.fn(),
  authenticateUser: jest.fn(),
});

const mockIdentityRepository = () => ({
  authenticateIdentity: jest.fn(),
});

const mockProfileRepository = () => ({
  createProfile: jest.fn(),
});

const mockRegistrationTokenRepository = () => ({
  createRegistrationToken: jest.fn(),
  findRegistrationToken: jest.fn(),
  deleteRegistrationToken: jest.fn(),
});

const mockRefreshTokenRepository = () => ({
  saveToken: jest.fn(),
  deleteToken: jest.fn(),
  findToken: jest.fn(),
});

const mockEmailService = () => ({
  sendVerificationEmail: jest.fn(),
});

const mockResetPasswordTokenRepository = () => ({
  createResetPasswordToken: jest.fn(),
  findResetPasswordToken: jest.fn(),
  deleteResetPasswordToken: jest.fn(),
  findTokenById: jest.fn(),
});

const mockConnection = () => ({
  save: jest.fn(),
  delete: jest.fn(),
  remove: jest.fn(),
});

const mockProjectService = () => ({
  archiveAllProjects: jest.fn(),
});

const mockCmsService = () => ({});

const mockEcomailService = () => ({
  addUserToMailingList: jest.fn(),
});

const mockMailchimpService = () => ({
  addUserToMailingList: jest.fn(),
});

describe('AuthService', () => {
  let service: AuthService;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let userRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let identityRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let profileRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let registrationTokenRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let refreshTokenRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let emailService: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let resetPasswordTokenRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let jwtService: JwtService;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let connection: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let projectService: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let cmsService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [JwtModule.register(jwtConfig)],
      providers: [
        AuthService,
        {
          provide: UserRepository,
          useFactory: mockUserRepository,
        },
        {
          provide: IdentityRepository,
          useFactory: mockIdentityRepository,
        },
        {
          provide: ProfileRepository,
          useFactory: mockProfileRepository,
        },
        {
          provide: RegistrationTokenRepository,
          useFactory: mockRegistrationTokenRepository,
        },
        {
          provide: RefreshTokenRepository,
          useFactory: mockRefreshTokenRepository,
        },
        {
          provide: EmailService,
          useFactory: mockEmailService,
        },
        {
          provide: ResetPasswordTokenRepository,
          useFactory: mockResetPasswordTokenRepository,
        },
        {
          provide: DataSource,
          useFactory: mockConnection,
        },
        {
          provide: ProjectService,
          useFactory: mockProjectService,
        },
        {
          provide: CmsService,
          useFactory: mockCmsService,
        },
        {
          provide: EcomailService,
          useFactory: mockEcomailService,
        },
        {
          provide: MailchimpService,
          useFactory: mockMailchimpService,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    jwtService = module.get<JwtService>(JwtService);
    userRepository = module.get<UserRepository>(UserRepository);
    identityRepository = module.get<IdentityRepository>(IdentityRepository);
    profileRepository = module.get<ProfileRepository>(ProfileRepository);
    registrationTokenRepository = module.get<RegistrationTokenRepository>(RegistrationTokenRepository);
    refreshTokenRepository = module.get<RefreshTokenRepository>(RefreshTokenRepository);
    emailService = module.get<EmailService>(EmailService);
    resetPasswordTokenRepository = module.get<ResetPasswordTokenRepository>(ResetPasswordTokenRepository);
    connection = module.get<DataSource>(DataSource);
    projectService = module.get<ProjectService>(ProjectService);
    cmsService = module.get<CmsService>(CmsService);
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  it('jwtService should be defined', () => {
    expect(jwtService).toBeDefined();
  });

  it('userRepository should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  it('identityRepository should be defined', () => {
    expect(identityRepository).toBeDefined();
  });

  it('connection should be defined', () => {
    expect(connection).toBeDefined();
  });

  it('profileRepository should be defined', () => {
    expect(profileRepository).toBeDefined();
  });

  it('registrationTokenRepository should be defined', () => {
    expect(registrationTokenRepository).toBeDefined();
  });

  it('refreshTokenRepository should be defined', () => {
    expect(refreshTokenRepository).toBeDefined();
  });

  it('emailService should be defined', () => {
    expect(emailService).toBeDefined();
  });

  it('resetPasswordTokenRepository should be defined', () => {
    expect(resetPasswordTokenRepository).toBeDefined();
  });

  it('project service should be defined', () => {
    expect(projectService).toBeDefined();
  });

  it('cms service should be defined', () => {
    expect(cmsService).toBeDefined();
  });

  describe('authflow', () => {
    it('should get user and return it', async () => {
      const user = userFactory();
      userRepository.getUserById.mockResolvedValue(user);
      const result = await service.getUser(user.id);
      expect(result).toEqual(user);
    });

    it('should throw BadRequestException if the user does not exist', async () => {
      userRepository.getUserById.mockResolvedValue(undefined);
      await expect(service.getUser('this-id-does-not-really-exist')).rejects.toThrow(
        new BadRequestException('User ID this-id-does-not-really-exist does not exist!')
      );
    });

    it('should get credentials for the user', async () => {
      const identity = identityFactory();
      const { accessToken, refreshToken } = await service.getCredentials(identity);
      expect(accessToken).toBeTruthy();
      expect(refreshToken).toBeTruthy();
    });

    it('should get Identity on the callback', async () => {
      const identity = identityFactory();
      const user = userFactory();
      userRepository.authenticateUser.mockResolvedValue(user);
      identityRepository.authenticateIdentity.mockResolvedValue(identity);
      const result = await service.getIdentityOnCallback(user.firstName, user.lastName, user.email, user.profilePicture, identity.provider);
      expect(result).toBeTruthy();
      expect(result).toEqual(identity);
    });

    it('should throw InternalServerError if the server is unable to fetch the Identity', async () => {
      const identity = identityFactory();
      const user = userFactory();
      userRepository.authenticateUser.mockResolvedValue(user);
      identityRepository.authenticateIdentity.mockImplementation(() => {
        throw new Error();
      });
      await expect(
        service.getIdentityOnCallback(user.firstName, user.lastName, user.email, user.profilePicture, identity.provider)
      ).rejects.toThrow(new InternalServerErrorException(`Unable to fetch Identity for authentication on: Error`));
    });
  });

  describe('cache', () => {
    it('should return the user entity', async () => {
      service.onApplicationBootstrap();
      const user = userFactory();
      userRepository.getUserById.mockResolvedValue(user);
      const result = await service.getOrSetAuthCache(user.id);
      expect(result).toEqual(user);
    });

    it('should throw InternalServerErrorException if cache is undefined', async () => {
      await expect(service.getOrSetAuthCache('this-id-does-not-really-exist')).rejects.toThrow(
        new InternalServerErrorException('Cache is undefined!')
      );
    });

    it('should throw UnauthorizedException if the user is not found in DB', async () => {
      // cache is private variable to the service
      service.onApplicationBootstrap();
      userRepository.getUserById.mockResolvedValue(undefined);
      await expect(service.getOrSetAuthCache('this-id-does-not-really-exist')).rejects.toThrow(
        new UnauthorizedException('User does not exist!')
      );
    });
  });
});
