import { InputType, Field } from '@nestjs/graphql';
import { IsString } from 'class-validator';

@InputType()
export class CreateUserProfileInput {
  @IsString()
  @Field()
  region: string;

  @IsString({ each: true })
  @Field(() => [String], { nullable: true })
  skills?: string[];

  @IsString()
  @Field()
  firstName: string;

  @IsString()
  @Field()
  lastName: string;

  @Field({ nullable: true })
  profilePicture?: string;
}
