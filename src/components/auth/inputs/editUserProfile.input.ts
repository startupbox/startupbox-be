import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class EditUserProfileInput {
  @Field({ nullable: true })
  region?: string;

  @Field(() => [String], { nullable: true })
  skills?: string[];

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field({ nullable: true })
  profilePicture?: string;
}
