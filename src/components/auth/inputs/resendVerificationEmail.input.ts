import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class ResetVerificationInput {
  @Field({ description: 'Request ID returned from the register mutation' })
  requestId: string;
}
