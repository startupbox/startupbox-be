import { Field, InputType } from '@nestjs/graphql';
import { MinLength } from 'class-validator';

@InputType()
export class ResetPasswordInputType {
  @Field()
  id: string;

  @Field()
  code: string;

  @MinLength(5)
  @Field()
  password: string;
}
