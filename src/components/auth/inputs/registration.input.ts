import { InputType, Field } from '@nestjs/graphql';
import { Equals, IsEmail, MinLength } from 'class-validator';

@InputType()
export class RegisterInputType {
  @IsEmail()
  @Field()
  email: string;

  @MinLength(5)
  @Field()
  password: string;

  @Equals(true)
  @Field()
  agreedToTermsAndConditions: boolean;
}
