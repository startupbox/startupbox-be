import { InputType, Field } from '@nestjs/graphql';

@InputType()
class OnboardingInputs {
  @Field()
  question: string;

  @Field(() => [String])
  answer: string[];
}

@InputType()
export class OnboardingInput {
  @Field(() => [OnboardingInputs])
  onboardingAnswers: OnboardingInputs[];

  @Field()
  result: string;
}
