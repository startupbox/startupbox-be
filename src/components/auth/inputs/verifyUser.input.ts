import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class VerifyUserInputType {
  @Field({ description: 'Request ID returned from the register mutation' })
  requestId: string;

  @Field({ description: 'Verification token received via email' })
  verificationToken: string;
}
