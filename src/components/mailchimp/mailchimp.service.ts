import client from '@mailchimp/mailchimp_marketing';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { ConfigKeys } from '../../config/configKeys.enum';

@Injectable()
export class MailchimpService {
  constructor(
    @Inject(ConfigService)
    private readonly config: ConfigService
  ) {}

  async addUserToMailingList(email: string, firstName: string, lastName: string): Promise<void> {
    try {
      client.setConfig({ server: this.config.get(ConfigKeys.MAILCHIMP_SERVER), apiKey: this.config.get(ConfigKeys.MAILCHIMP_API_KEY) });

      await client.lists.addListMember(this.config.get(ConfigKeys.MAILCHIMP_SUBSCRIBERS_LIST_ID), {
        email_address: email,
        status: 'subscribed',
        merge_fields: {
          FNAME: firstName,
          LNAME: lastName,
        },
      });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('MailchimpService: Failed to add user to mailing list', error);
    }
  }
}
