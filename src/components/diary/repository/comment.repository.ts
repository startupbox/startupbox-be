import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { UserEntity } from '../../auth/entity/user.entity';
import { CommentEntity } from '../entity/comment.entity';
import { TaskEntity } from '../entity/task.entity';

@Injectable()
export class CommentRepository extends Repository<CommentEntity> {
  constructor(private dataSource: DataSource) {
    super(CommentEntity, dataSource.createEntityManager());
  }

  async createComment(text: string, task: TaskEntity, user: UserEntity): Promise<CommentEntity> {
    const c = new CommentEntity();
    c.text = text;
    c.task = task;
    c.user = user;
    return this.save(c);
  }
}
