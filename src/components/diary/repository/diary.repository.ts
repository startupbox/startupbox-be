import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProjectEntity } from '../../project/entity/project.entity';
import { DiaryEntity } from '../entity/diary.entity';
import { DiaryStatus } from '../interfaces/diaryStatus.enum';

@Injectable()
export class DiaryRepository extends Repository<DiaryEntity> {
  constructor(private dataSource: DataSource) {
    super(DiaryEntity, dataSource.createEntityManager());
  }

  async createDiary(project: ProjectEntity, status: DiaryStatus, projectPhase: string): Promise<DiaryEntity> {
    const d = new DiaryEntity();
    d.status = status;
    d.project = project;
    d.projectPhase = projectPhase;
    return this.save(d);
  }

  async getDiaryByProjectId(projectId: string): Promise<DiaryEntity | undefined> {
    const diary = await this.createQueryBuilder('diary')
      .where('diary.archived = :archived', { archived: false })
      .leftJoinAndSelect('diary.project', 'project')
      .where('project.archived = :archived', { archived: false })
      .andWhere('diary.projectId = :projectId', { projectId })
      .leftJoinAndSelect('project.user', 'createdBy')
      .leftJoinAndSelect('diary.tasks', 'task')
      .leftJoinAndSelect('task.comments', 'comment')
      .leftJoinAndSelect('comment.user', 'user')
      .getOne();
    return diary;
  }

  async getAllDiaries(): Promise<DiaryEntity[]> {
    const diary = await this.createQueryBuilder('diary')
      .where('diary.archived = :archived', { archived: false })
      .leftJoinAndSelect('diary.tasks', 'task')
      .getMany();
    return diary;
  }

  async completeDiary(id: string): Promise<void> {
    await this.update({ id }, { status: DiaryStatus.DONE });
    return;
  }

  async deleteDiary(diary: DiaryEntity): Promise<DiaryEntity> {
    diary.archived = true;
    return await this.save(diary);
  }
}
