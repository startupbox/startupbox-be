import { Injectable } from '@nestjs/common';
import moment from 'moment';
import { DataSource, Repository } from 'typeorm';
import { DiaryEntity } from '../entity/diary.entity';
import { TaskEntity } from '../entity/task.entity';
import { CmsTask } from '../interfaces/cmsTask.interface';
import { DiaryScenario } from '../interfaces/scenario.interface';

@Injectable()
export class TaskRepository extends Repository<TaskEntity> {
  constructor(private dataSource: DataSource) {
    super(TaskEntity, dataSource.createEntityManager());
  }

  async createInitialGeneralTasks(diary: DiaryEntity, scenario: DiaryScenario): Promise<TaskEntity[]> {
    const aggregated = [];
    for (let i = 0; i < scenario.phases.length; i++) {
      for (let j = 0; j < scenario.phases[i].components.length; j++) {
        const tasks = await this.generateTaskObjects(scenario.phases[i].components[j].tasks, diary);
        aggregated.push(...tasks);
      }
    }
    return this.save(aggregated);
  }

  async getBasicTaskById(id: string): Promise<TaskEntity | undefined> {
    const task = await this.createQueryBuilder('task').where('task.id = :id', { id }).getOne();
    return task;
  }

  async getTaskById(id: string): Promise<TaskEntity | undefined> {
    const task = await this.createQueryBuilder('task')
      .where('task.id = :id', { id })
      .leftJoinAndSelect('task.comments', 'comment')
      .leftJoinAndSelect('comment.user', 'user')
      .getOne();
    return task;
  }

  async createTask(pointer: string, diaryId?: string): Promise<TaskEntity> {
    const t = new TaskEntity();
    t.pointer = pointer;
    t.completed = false;
    t.hasNewUpdate = false;
    if (diaryId) {
      t.diaryId = diaryId;
    }
    return this.save(t);
  }

  async updateTask(task: TaskEntity, links: string[], completed: boolean, hasNewUpdate: boolean): Promise<TaskEntity> {
    if (links) {
      task.links ? task.links.push(...links.filter((val: string) => !task.links.includes(val))) : (task.links = links);
    }
    task.completed = completed;
    task.hasNewUpdate = hasNewUpdate;
    if (completed) {
      task.completedAt = moment().toDate();
    }
    await this.update({ id: task.id }, task);
    return task;
  }

  private async generateTaskObjects(tasks: CmsTask[], diary: DiaryEntity): Promise<TaskEntity[]> {
    const checkboxes = [];
    for (const task of tasks) {
      const t = new TaskEntity();
      t.pointer = task.id;
      t.completed = false;
      t.hasNewUpdate = false;
      t.diaryId = diary.id;
      checkboxes.push(t);
    }
    return checkboxes;
  }
}
