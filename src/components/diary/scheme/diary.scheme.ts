export interface Phase {
  label: string;
  components?: Component[] | null;
  id?: string | null;
}
export interface Component {
  label: string;
  title: string;
  tasks?: Task[] | null;
  id?: string | null;
}
export interface Task {
  dbPointer: string;
  title: string;
  order: number;
  enabled: boolean;
  howToPage?: string | null;
}
