import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, ManyToOne, Column, OneToMany } from 'typeorm';
import { CommentEntity } from './comment.entity';
import { DiaryEntity } from './diary.entity';

@Entity()
export class TaskEntity extends BasicEntity {
  @Column()
  pointer: string;

  @Column()
  hasNewUpdate: boolean;

  @OneToMany(() => CommentEntity, (comment: CommentEntity) => comment.task, { nullable: true })
  comments?: CommentEntity[];

  @Column('simple-array', { nullable: true })
  links?: string[];

  @Column()
  completed: boolean;

  @ManyToOne(() => DiaryEntity, (diary: DiaryEntity) => diary.tasks, { onDelete: 'CASCADE', nullable: true })
  diary: DiaryEntity;

  @Column({ nullable: true })
  diaryId: string;

  @Column({ nullable: true })
  completedAt: Date;
}
