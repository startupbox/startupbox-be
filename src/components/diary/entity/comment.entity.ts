import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, ManyToOne, JoinColumn, Column } from 'typeorm';
import { UserEntity } from '../../../components/auth/entity/user.entity';
import { TaskEntity } from './task.entity';

@Entity()
export class CommentEntity extends BasicEntity {
  @Column()
  text: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.comments, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;

  @Column()
  userId: string;

  @ManyToOne(() => TaskEntity, (task: TaskEntity) => task.comments, { onDelete: 'CASCADE' })
  @JoinColumn()
  task: TaskEntity;

  @Column()
  taskId: string;
}
