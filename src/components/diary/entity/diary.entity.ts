import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, Column, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { ProjectEntity } from '../../../components/project/entity/project.entity';
import { DiaryStatus } from '../interfaces/diaryStatus.enum';
import { TaskEntity } from './task.entity';

@Entity()
export class DiaryEntity extends BasicEntity {
  @Column('text')
  status: DiaryStatus;

  @ManyToOne(() => ProjectEntity, (project: ProjectEntity) => project.diary)
  @JoinColumn()
  project: ProjectEntity;

  @Column()
  projectId: string;

  @OneToMany(() => TaskEntity, (task: TaskEntity) => task.diary, { nullable: true })
  tasks: TaskEntity[];

  @Column({ default: false })
  archived: boolean;

  @Column({ nullable: true })
  projectPhase: string;
}
