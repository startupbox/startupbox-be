import { registerEnumType } from '@nestjs/graphql';

export enum ComponentStage {
  TEAM = 'TEAM',
  VALUE_PROPOSITION = 'VALUE_PROPOSITION',
  PRODUCT = 'PRODUCT',
  BUSINESS_MODEL = 'BUSINESS_MODEL',
  INVESTMENT = 'INVESTMENT',
  MARKETING_AND_SALES = 'MARKETING_AND_SALES',
  COMPANY = 'COMPANY',
}

registerEnumType(ComponentStage, {
  name: 'ComponentStage',
});
