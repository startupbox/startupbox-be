export enum GraphcmsStage {
  PUBLISHED = 'PUBLISHED',
  DRAFT = 'DRAFT',
}
