import { Component } from './component.interface';
import { PhaseStage } from './phaseStage.enum';

export interface Phase {
  label: string;
  title: PhaseStage;
  language: string;
  components: Component[];
  tasksCount?: number;
  completedTasksCount?: number;
}
