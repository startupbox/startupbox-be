import { CmsTask } from './cmsTask.interface';

export interface Component {
  label: string;
  language: string;
  tasks: CmsTask[];
  title: string;
}
