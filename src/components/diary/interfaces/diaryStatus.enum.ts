import { registerEnumType } from '@nestjs/graphql';

export enum DiaryStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE',
}

registerEnumType(DiaryStatus, {
  name: 'DiaryStatus',
});
