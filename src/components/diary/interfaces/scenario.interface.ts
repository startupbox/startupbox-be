import { Phase } from './phase.interface';

export interface DiaryScenario {
  id: string;
  name: string;
  language: string;
  phases: Phase[];
}
