export interface CmsTask {
  id: string;
  title: string;
  language?: string;
  howToPage: string;
  completed?: boolean;
  subtitle?: string;
  question?: string;
  duration?: number;
  thumbnail?: {
    id: string;
    url: string;
    height: string;
    width: string;
  };
}
