import { registerEnumType } from '@nestjs/graphql';

export enum PhaseStage {
  IDEA = 'IDEA',
  PROTOTYPE = 'PROTOTYPE',
  MVP = 'MVP',
  BUSINESS = 'BUSINESS',
  IDEATION = 'IDEATION',
}

registerEnumType(PhaseStage, {
  name: 'PhaseStage',
});
