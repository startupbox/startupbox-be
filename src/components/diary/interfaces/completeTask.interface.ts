import { TaskEntity } from '../entity/task.entity';

export interface CompleteTaskInterface extends TaskEntity {
  title: string;
  howToPage: string;
  duration: number;
  subtitle: string;
  question: string;
  thumbnail: {
    id: string;
    url: string;
    height: string;
    width: string;
  };
}
