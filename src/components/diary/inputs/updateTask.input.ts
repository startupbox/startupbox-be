import { InputType, Field } from '@nestjs/graphql';
import { IsString, IsBoolean, IsOptional, IsUUID } from 'class-validator';

@InputType()
export class UpdateTaskInput {
  @IsUUID()
  @Field()
  taskId: string;

  @IsBoolean()
  @Field()
  completed: boolean;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  text?: string;

  @IsOptional()
  @IsString({ each: true })
  @Field(() => [String], { nullable: true })
  links?: string[];
}
