import { Test, TestingModule } from '@nestjs/testing';
import { DiaryResolver } from './diary.resolver';
import { DiaryService } from './diary.service';
import { ProjectService } from '../project/project.service';
import { CmsService } from '../cms/cms.service';

const mockDiaryService = () => ({});
const mockProjctService = () => ({});
const mockCmsService = () => ({});

describe('DiaryResolver', () => {
  let resolver: DiaryResolver;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let service: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let projectService: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let cmsService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DiaryResolver,
        {
          provide: DiaryService,
          useFactory: mockDiaryService,
        },
        {
          provide: ProjectService,
          useFactory: mockProjctService,
        },
        {
          provide: CmsService,
          useFactory: mockCmsService,
        },
      ],
    }).compile();

    resolver = module.get<DiaryResolver>(DiaryResolver);
    service = module.get<DiaryService>(DiaryService);
    projectService = module.get<ProjectService>(ProjectService);
    cmsService = module.get<CmsService>(CmsService);
  });

  it('resolver should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  it('project service should be defined', () => {
    expect(projectService).toBeDefined();
  });

  it('cms service should be defined', () => {
    expect(cmsService).toBeDefined();
  });
});
