/* eslint-disable prettier/prettier */
import { Injectable, BadRequestException } from '@nestjs/common';
import { DiaryRepository } from './repository/diary.repository';
import { DiaryEntity } from './entity/diary.entity';
import { ProjectEntity } from '../project/entity/project.entity';
import { DiaryStatus } from './interfaces/diaryStatus.enum';
import { TaskRepository } from './repository/task.repository';
import { CommentRepository } from './repository/comment.repository';
import { UserEntity } from '../auth/entity/user.entity';
import { TaskEntity } from './entity/task.entity';
import { CompleteTaskInterface } from './interfaces/completeTask.interface';
import _ from 'lodash';
import { DiaryScenario } from './interfaces/scenario.interface';
import { CmsService } from '../cms/cms.service';
import { Phase } from './interfaces/phase.interface';

@Injectable()
export class DiaryService {
  constructor(
    private readonly diaryRepository: DiaryRepository,
    private readonly taskRepository: TaskRepository,
    private readonly commentRepository: CommentRepository,
    private readonly cmsService: CmsService,
  ) {}

  async createDiary(project: ProjectEntity, status: DiaryStatus, scenario: DiaryScenario, projectPhase?: string): Promise<DiaryEntity> {
    const diary = await this.diaryRepository.createDiary(project, status, projectPhase);
    await this.taskRepository.createInitialGeneralTasks(diary, scenario);
    return await this.diaryRepository.getDiaryByProjectId(project.id);
  }

  async createTask(pointer: string, diaryId: string): Promise<TaskEntity> {
   return await this.taskRepository.createTask(pointer, diaryId)
  }

  async getDiaryByProjectId(projectId: string): Promise<DiaryEntity> {
    const diary = await this.diaryRepository.getDiaryByProjectId(projectId);

    if (!diary) {
      throw new BadRequestException(`Diary of Project ID ${projectId} does not exist!`);
    }

    return diary;
  }

  async getTaskById(taskId: string): Promise<CompleteTaskInterface> {
    const task = await this.taskRepository.getTaskById(taskId);
    const cmsTask = await this.cmsService.findTaskFromCms(task.pointer);

    if (!task || !cmsTask) {
      throw new BadRequestException(`Task ID ${taskId} does not exist!`);
    }

    if (task.comments) {
      task.comments = task.comments.sort((a, b) => {
        const dateA = new Date(a.createdAt);
        const dateB = new Date(b.createdAt);
        return dateB > dateA ? 1 : -1;
      });
    }
    return { ...task, ...{ title: cmsTask.title, howToPage: cmsTask.howToPage, duration: cmsTask.duration, subtitle: cmsTask.subtitle, question: cmsTask.question, thumbnail: cmsTask.thumbnail }} as CompleteTaskInterface;
  }

  async viewTask(taskId: string): Promise<CompleteTaskInterface> {
    const task = await this.getTaskById(taskId);
    const taskFromCms = await this.cmsService.findTaskFromCms(task.pointer);
    const result = await this.taskRepository.updateTask(
      _.pick(task, ['id', 'pointer', 'hasNewUpdate', 'links', 'completed']) as TaskEntity,
      task.links,
      task.completed,
      false
    );

      return { ..._.merge(result, task), ...{ title: taskFromCms.title, howToPage: taskFromCms.howToPage, duration: taskFromCms.duration, subtitle: taskFromCms.subtitle, question: taskFromCms.question, thumbnail: taskFromCms.thumbnail }} as CompleteTaskInterface;
  }

  async updateTaskById(
    taskId: string,
    completed: boolean,
    user?: UserEntity,
    text?: string,
    links?: string[]
  ): Promise<CompleteTaskInterface> {
    const task = await this.taskRepository.getBasicTaskById(taskId);
    let hasUpdate = false;
    if (!task) {
      throw new BadRequestException(`Task ID ${taskId} does not exist!`);
    }
    if (text && user) {
      const comment = await this.commentRepository.createComment(text, task, user);
      if(comment) hasUpdate = true;
    }
    await this.taskRepository.updateTask(task, links, completed, hasUpdate);
    return await this.getTaskById(taskId);
  }

  async deleteDiary(diary: DiaryEntity): Promise<DiaryEntity> {
    try {
      return await this.diaryRepository.deleteDiary(diary);
    } catch (e) {
      throw new BadRequestException('Something went wrong');
    }
  }

  async combineTasksAndPhases(scenario: DiaryScenario, diary: DiaryEntity): Promise<Phase[]> {
    // check if there are any new tasks from cms that are not stored in DB
    const cmsTaskIds: string[] = [];
    scenario.phases.forEach((phase) => phase.components.map((component) => component.tasks.map((task) => cmsTaskIds.push(task.id))));
    const diaryTaskIds: string[] = [];
    diary.tasks.forEach((task) => diaryTaskIds.push(task.pointer));
    const differences = cmsTaskIds.filter((x) => !diaryTaskIds.includes(x));

    if (differences.length > 0) {
      differences.forEach(async (diff) => {
        await this.createTask(diff, diary.id);
      });
    }

    // match tasks from cms to progress from the DB
    scenario.phases.forEach((phase) => {
      phase.components.forEach((component) => {
        const taskCount = phase.components.map((component) => component.tasks.length).reduce((phaseTasks, acc) => phaseTasks + acc, 0);
        phase.tasksCount = taskCount;
        for (let i = 0; i < component.tasks.length; i++) {
          for (let j = 0; j < diary.tasks.length; j++) {
            if (component.tasks[i].id === diary.tasks[j].pointer) {
              component.tasks[i] = {
                ...component.tasks[i],
                ...diary.tasks[j],
              };
            }
          }
        }
        const completedTasks = phase.components
          .map((component) => component.tasks.filter((task) => task.completed === true).length)
          .reduce((phaseTasks, acc) => phaseTasks + acc, 0);
        phase.completedTasksCount = completedTasks;
      });
    });

    if (differences.length === 0) {
      return scenario.phases;
    }
  }
}
