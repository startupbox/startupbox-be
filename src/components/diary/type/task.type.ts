import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { CommentType } from './comment.type';
import { Thumbnail } from './thumbnail.type';

@ObjectType('Task')
export class TaskType extends BasicType {
  @Field()
  title: string;

  @Field(() => [CommentType], { nullable: true })
  comments?: CommentType[];

  @Field(() => [String], { nullable: true })
  links?: string[];

  @Field()
  completed: boolean;

  @Field()
  hasNewUpdate: boolean;

  @Field()
  howToPage: string;

  @Field()
  pointer: string;

  @Field()
  diaryId: string;

  @Field({ nullable: true })
  question?: string;

  @Field({ nullable: true })
  subtitle: string;

  @Field({ nullable: true })
  duration: number;

  @Field({ nullable: true })
  thumbnail?: Thumbnail;

  @Field({ nullable: true })
  completedAt?: Date;
}
