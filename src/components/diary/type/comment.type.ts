import { BasicType } from '../../../utils/basicType';
import { ObjectType, Field } from '@nestjs/graphql';
import { UserType } from '../../../components/auth/type/user.type';

@ObjectType('Comment')
export class CommentType extends BasicType {
  @Field()
  text: string;

  @Field(() => UserType)
  user: UserType;
}
