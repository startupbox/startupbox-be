import { ObjectType, Field } from '@nestjs/graphql';
import { PhaseStage } from '../interfaces/phaseStage.enum';
import { ComponentType } from './component.type';

@ObjectType('Phase')
export class PhaseType {
  @Field(() => PhaseStage)
  title: PhaseStage;

  @Field()
  label: string;

  @Field(() => [ComponentType])
  components: ComponentType[];

  @Field({ nullable: true })
  tasksCount?: number;

  @Field({ nullable: true })
  completedTasksCount?: number;
}
