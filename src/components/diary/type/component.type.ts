import { ObjectType, Field } from '@nestjs/graphql';
import { TaskType } from './task.type';

@ObjectType('Component')
export class ComponentType {
  @Field()
  label: string;

  @Field({ nullable: true })
  title?: string;

  @Field(() => [TaskType])
  tasks: TaskType[];
}
