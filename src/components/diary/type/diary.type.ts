import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { DiaryStatus } from '../interfaces/diaryStatus.enum';
import { ProjectType } from '../../../components/project/type/project.type';
import { PhaseType } from './phase.type';

@ObjectType('Diary')
export class DiaryType extends BasicType {
  @Field(() => DiaryStatus)
  status: DiaryStatus;

  @Field(() => ProjectType)
  project: ProjectType;

  @Field(() => [PhaseType])
  phases: PhaseType[];

  @Field()
  archived: boolean;

  @Field(() => [String])
  recommendedTasks: string[];

  @Field({ nullable: true })
  projectPhase?: string;
}
