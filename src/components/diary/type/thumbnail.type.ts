import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType('Thumbnail')
export class Thumbnail {
  @Field()
  id: string;

  @Field()
  url: string;

  @Field()
  height: string;

  @Field()
  width: string;
}
