import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiaryService } from './diary.service';
import { DiaryResolver } from './diary.resolver';
import { DiaryRepository } from './repository/diary.repository';
import { TaskRepository } from './repository/task.repository';
import { CommentRepository } from './repository/comment.repository';
import { ProjectService } from '../project/project.service';
import { ProjectRepository } from '../project/repository/project.repository';
import { CmsService } from '../cms/cms.service';
import { ProjectEntity } from '../project/entity/project.entity';
import { CommentEntity } from './entity/comment.entity';
import { DiaryEntity } from './entity/diary.entity';
import { TaskEntity } from './entity/task.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DiaryEntity, TaskEntity, CommentEntity, ProjectEntity])],
  providers: [
    DiaryService,
    DiaryResolver,
    ProjectService,
    CmsService,
    DiaryRepository,
    TaskRepository,
    CommentRepository,
    ProjectRepository,
  ],
  exports: [DiaryService],
})
export class DiaryModule {}
