import { Test, TestingModule } from '@nestjs/testing';
import { DiaryService } from './diary.service';
import { DiaryRepository } from './repository/diary.repository';
import { TaskRepository } from './repository/task.repository';
import { CommentRepository } from './repository/comment.repository';
import { ConfigService } from '../../config/config.service';
import { CmsService } from '../cms/cms.service';

const mockDiaryRepository = () => ({});
const mockTaskRepository = () => ({});
const mockCommentRepository = () => ({});
const mockConfigService = () => ({});
const mockCmsService = () => ({});

describe('DiaryService', () => {
  let service: DiaryService;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let diaryRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let taskRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let commentRepository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let configService: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let cmsService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DiaryService,
        {
          provide: DiaryRepository,
          useFactory: mockDiaryRepository,
        },
        {
          provide: TaskRepository,
          useFactory: mockTaskRepository,
        },
        {
          provide: CommentRepository,
          useFactory: mockCommentRepository,
        },
        {
          provide: ConfigService,
          useFactory: mockConfigService,
        },
        {
          provide: CmsService,
          useFactory: mockCmsService,
        },
      ],
    }).compile();

    service = module.get<DiaryService>(DiaryService);
    diaryRepository = module.get<DiaryRepository>(DiaryRepository);
    taskRepository = module.get<TaskRepository>(TaskRepository);
    commentRepository = module.get<CommentRepository>(CommentRepository);
    configService = module.get<ConfigService>(ConfigService);
    cmsService = module.get<CmsService>(CmsService);
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  it('diaryRepository should be defined', () => {
    expect(diaryRepository).toBeDefined();
  });

  it('taskRepository should be defined', () => {
    expect(taskRepository).toBeDefined();
  });

  it('commentRepository should be defined', () => {
    expect(commentRepository).toBeDefined();
  });

  it('configService should be defined', () => {
    expect(configService).toBeDefined();
  });

  it('cmsService should be defined', () => {
    expect(cmsService).toBeDefined();
  });
});
