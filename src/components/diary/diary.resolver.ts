import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { UseGuards, ParseUUIDPipe, BadRequestException } from '@nestjs/common';
import { DiaryService } from './diary.service';
import { GqlAuthGuard } from '../auth/utils/gqlAuth.guard';
import { GetUser } from '../auth/utils/getUser.decorator';
import { DiaryType } from './type/diary.type';
import { TaskType } from './type/task.type';
import { DiaryEntity } from './entity/diary.entity';
import { UserEntity } from '../auth/entity/user.entity';
import { UpdateTaskInput } from './inputs/updateTask.input';
import { CompleteTaskInterface } from './interfaces/completeTask.interface';
import { ProjectService } from '../project/project.service';
import { PhaseType } from './type/phase.type';
import { Phase } from './interfaces/phase.interface';
import { CmsService } from '../cms/cms.service';

@Resolver(() => DiaryType)
export class DiaryResolver {
  constructor(
    private readonly diaryService: DiaryService,
    private readonly projectService: ProjectService,
    private readonly cmsService: CmsService
  ) {}

  @Query(() => DiaryType)
  @UseGuards(GqlAuthGuard)
  async getDiaryByProjectId(@Args('projectId', ParseUUIDPipe) projectId: string): Promise<DiaryEntity> {
    return await this.diaryService.getDiaryByProjectId(projectId);
  }

  @Query(() => TaskType)
  @UseGuards(GqlAuthGuard)
  async getTaskById(@Args('taskId', ParseUUIDPipe) taskId: string): Promise<CompleteTaskInterface> {
    return await this.diaryService.getTaskById(taskId);
  }

  @Query(() => TaskType)
  @UseGuards(GqlAuthGuard)
  async viewTask(@Args('taskId', ParseUUIDPipe) taskId: string): Promise<CompleteTaskInterface> {
    return await this.diaryService.viewTask(taskId);
  }

  @Mutation(() => TaskType)
  @UseGuards(GqlAuthGuard)
  async updateTaskById(
    @GetUser() user: UserEntity,
    @Args('updateTaskInput') updateTaskInput: UpdateTaskInput
  ): Promise<CompleteTaskInterface> {
    const { taskId, text, links, completed } = updateTaskInput;
    return await this.diaryService.updateTaskById(taskId, completed, user, text, links);
  }

  @ResolveField(() => [PhaseType])
  async phases(@Parent() diary: DiaryEntity): Promise<Phase[]> {
    const project = await this.projectService.getProjectById(diary.projectId);
    const scenario = await this.cmsService.fetchScenarioById(project.scenarioId);

    if (!scenario) {
      throw new BadRequestException(`Scenario of project with id: ${diary.projectId} does not exist!`);
    }

    try {
      return await this.diaryService.combineTasksAndPhases(scenario, diary);
    } catch {
      throw new BadRequestException('Something went wrong');
    }
  }

  @ResolveField(() => [String])
  async recommendedTasks(@Parent() diary: DiaryEntity): Promise<string[]> {
    const onboardingResult = diary.project.user.onboardingResult ? diary.project.user.onboardingResult : 'Default';
    const recommendedTasks = diary.projectPhase ? diary.projectPhase : onboardingResult;
    return await this.cmsService.fetchRecommendedTasks(recommendedTasks);
  }
}
