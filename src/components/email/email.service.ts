import Mailgun from 'mailgun.js';
import FormData from 'form-data';
import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { ConfigKeys } from '../../config/configKeys.enum';
import { RegistrationTokenEntity } from '../auth/entity/registrationToken.entity';
import { getServerBaseUri } from '../auth/utils/uri.utils';
import { ResetPasswordTokenEntity } from '../auth/entity/resetPasswordToken.entity';

@Injectable()
export class EmailService {
  constructor(
    @Inject(ConfigService)
    private readonly config: ConfigService
  ) {}

  async sendVerificationEmail(token: RegistrationTokenEntity, domain: string): Promise<void> {
    const client = new Mailgun(FormData).client({
      username: 'api',
      key: this.config.get(ConfigKeys.MAILGUN_API_KEY),
      url: 'https://api.eu.mailgun.net',
    });

    await client.messages.create(this.config.get(ConfigKeys.MAILGUN_DOMAIN), {
      from: `Startupbox <info@${this.config.get(ConfigKeys.MAILGUN_DOMAIN)}>`,
      to: token.email,
      subject: 'Potvrzení e-mailu',
      template: 'verification',
      'h:X-Mailgun-Variables': JSON.stringify({
        verificationUrl: `${getServerBaseUri(true, domain)}/auth/verify-email/${token.id}/${token.verificationToken}`,
      }),
    });
  }

  async sendResetPasswordEmail(token: ResetPasswordTokenEntity, domain: string): Promise<void> {
    const client = new Mailgun(FormData).client({
      username: 'api',
      key: this.config.get(ConfigKeys.MAILGUN_API_KEY),
      url: 'https://api.eu.mailgun.net',
    });

    await client.messages.create(this.config.get(ConfigKeys.MAILGUN_DOMAIN), {
      from: `Startupbox <info@${this.config.get(ConfigKeys.MAILGUN_DOMAIN)}>`,
      to: token.user.email,
      subject: 'Password reset',
      template: 'password-reset',
      'h:X-Mailgun-Variables': JSON.stringify({
        code: token.code,
        passwordResetUrl: `${getServerBaseUri(true, domain)}/auth/password-reset/${token.id}`,
      }),
    });
  }
}
