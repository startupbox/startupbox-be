import { UseGuards } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { UserRole } from '../auth/interfaces/userRole.enum';
import { AllowedRoles } from '../auth/utils/allowedRoles.decorator';
import { GqlAuthGuard } from '../auth/utils/gqlAuth.guard';
import { RoleGuard } from '../auth/utils/roles.guard';
import { AdminService } from './admin.service';
import { AdminStatisticsType } from './type/adminStatistics.type';
import { GetUser } from '../auth/utils/getUser.decorator';
import { UserEntity } from '../auth/entity/user.entity';

@Resolver('Admin')
export class AdminResolver {
  constructor(private readonly adminService: AdminService) {}

  @Query(() => AdminStatisticsType)
  @UseGuards(GqlAuthGuard, RoleGuard)
  @AllowedRoles(UserRole.ADMIN, UserRole.SUPER_ADMIN)
  async getAdminStatistics(@GetUser() user: UserEntity, @Args('subdomain') subdomain: string): Promise<AdminStatisticsType> {
    if (user.role === UserRole.ADMIN) {
      if (user.subdomain !== subdomain) {
        throw Error('Forbidden');
      }
    }

    return await this.adminService.getAdminStatistics(subdomain);
  }
}
