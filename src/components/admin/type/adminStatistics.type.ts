import { Field, ObjectType } from '@nestjs/graphql';
import { CountType } from './count.type';
import { OnboardingAnswersType } from './onboardingAnswers.type';
import { RegistrationRegionsType } from './registrationRegions.type';
import { RegistrationsPerMonthType } from './registrationsPerMonth.type';
import { ActiveUsersType } from './activeUsers.type';

@ObjectType('AdminStatistics')
export class AdminStatisticsType {
  @Field()
  totalRegistedUsersCount: number;

  @Field(() => [CountType])
  onboardingResults: CountType[];

  @Field(() => [RegistrationRegionsType])
  registrationRegions: RegistrationRegionsType[];

  @Field(() => [RegistrationsPerMonthType])
  registrationsPerMonth: RegistrationsPerMonthType[];

  @Field(() => [OnboardingAnswersType])
  onboardingAnswers: OnboardingAnswersType[];

  @Field(() => [ActiveUsersType])
  activeUsers: ActiveUsersType[];
}
