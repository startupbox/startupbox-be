import { Field, ObjectType } from '@nestjs/graphql';
import { CountType } from './count.type';

@ObjectType('RegistrationsPerMonth')
export class RegistrationsPerMonthType {
  @Field()
  year: number;

  @Field(() => [CountType])
  months: CountType[];
}
