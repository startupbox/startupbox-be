import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('Count')
export class CountType {
  @Field()
  value: string;

  @Field()
  count: number;
}
