import { Field, ObjectType } from '@nestjs/graphql';
import { CountType } from './count.type';

@ObjectType('RegistrationRegions')
export class RegistrationRegionsType {
  @Field()
  country: string;

  @Field(() => [CountType])
  regions: CountType[];
}
