import { Field, ObjectType } from '@nestjs/graphql';
import { CountType } from './count.type';

@ObjectType('ActiveUsers')
export class ActiveUsersType {
  @Field()
  year: number;

  @Field(() => [CountType])
  months: CountType[];
}
