import { Field, ObjectType } from '@nestjs/graphql';
import { CountType } from './count.type';

@ObjectType('OnboardingAnswers')
export class OnboardingAnswersType {
  @Field()
  question: string;

  @Field(() => [CountType])
  answers: CountType[];
}
