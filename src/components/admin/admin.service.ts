import { Injectable } from '@nestjs/common';
import { UserRepository } from '../auth/repository/user.repository';
import { RegistrationRegionsType } from './type/registrationRegions.type';
import { OnboardingAnswersType } from './type/onboardingAnswers.type';
import { RegistrationsPerMonthType } from './type/registrationsPerMonth.type';
import { AdminStatisticsType } from './type/adminStatistics.type';
import { UserEntity } from '../auth/entity/user.entity';
import { CountType } from './type/count.type';
import { ActiveUsersType } from './type/activeUsers.type';

type CountRecord = Record<string, Record<string, number>>;

const countryRegion = {
  'Hlavní město Praha': 'CZ',
  'Jihočeský kraj': 'CZ',
  'Moravskoslezský kraj': 'CZ',
  'Ústecký kraj': 'CZ',
  'Olomoucký kraj': 'CZ',
  'Plzeňský kraj': 'CZ',
  'Středočeský kraj': 'CZ',
  'Liberecký kraj': 'CZ',
  'Karlovarský kraj': 'CZ',
  'Jihomoravský kraj': 'CZ',
  'Kraj Vysočina': 'CZ',
  'Zlínský kraj': 'CZ',
  'Královéhradecký kraj': 'CZ',
  'Pardubický kraj': 'CZ',
  Bratislavský: 'SK',
  Trenčínský: 'SK',
  Banskobystrický: 'SK',
  Žilinský: 'SK',
  Trnavský: 'SK',
  Nitranský: 'SK',
  Košický: 'SK',
  Prešovský: 'SK',
};

const months: string[] = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

@Injectable()
export class AdminService {
  constructor(private readonly userRepository: UserRepository) {}

  async getAdminStatistics(subdomain: string): Promise<AdminStatisticsType> {
    const allUsersWithProfiles = await this.userRepository.getAllUsers(subdomain);
    const activityStats = await this.userRepository.getUsersActivity(subdomain);

    return {
      totalRegistedUsersCount: allUsersWithProfiles.length,
      onboardingResults: getOnboardingResults(allUsersWithProfiles),
      registrationRegions: getRegistrationRegions(allUsersWithProfiles),
      registrationsPerMonth: getRegistrationsPerMonth(allUsersWithProfiles),
      onboardingAnswers: getOnboardingAnswers(allUsersWithProfiles),
      activeUsers: getActiveUsers(activityStats),
    };
  }
}

const getOnboardingResults = (users: UserEntity[]): CountType[] =>
  Object.entries(
    users.reduce((countRecord: Record<string, number>, user) => {
      if (!user.onboardingResult) {
        return countRecord;
      }

      if (!countRecord[user.onboardingResult]) {
        countRecord[user.onboardingResult] = 1;
      } else {
        countRecord[user.onboardingResult] = countRecord[user.onboardingResult] + 1;
      }

      return countRecord;
    }, {})
  ).map(([value, count]) => ({
    value,
    count,
  }));

const getRegistrationRegions = (users: UserEntity[]): RegistrationRegionsType[] =>
  Object.entries(
    users.reduce((countRecord: CountRecord, user) => {
      if (!user.profile?.region) {
        return countRecord;
      }

      const country = countryRegion[user.profile.region] ?? 'other';

      if (!countRecord[country]) {
        countRecord[country] = {};
      }

      if (!countRecord[country][user.profile.region]) {
        countRecord[country][user.profile.region] = 1;
      } else {
        countRecord[country][user.profile.region] = countRecord[country][user.profile.region] + 1;
      }

      return countRecord;
    }, {})
  ).map(([country, count]) => ({
    country,
    regions: Object.entries(count).map(([value, count]) => ({
      value,
      count,
    })),
  }));

const getRegistrationsPerMonth = (users: UserEntity[]): RegistrationsPerMonthType[] =>
  Object.entries(
    users.reduce((countRecord: CountRecord, user) => {
      const year = user.createdAt.getFullYear();
      const month = user.createdAt.toLocaleString('default', { month: 'long' });

      if (!countRecord[year]) {
        countRecord[year] = {};
      }

      if (!countRecord[year][month]) {
        countRecord[year][month] = 1;
      } else {
        countRecord[year][month] = countRecord[year][month] + 1;
      }

      return countRecord;
    }, {})
  ).map(([year, count]) => ({
    year: parseInt(year),
    months: Object.entries(count).map(([value, count]) => ({
      value,
      count,
    })),
  }));

const getOnboardingAnswers = (users: UserEntity[]): OnboardingAnswersType[] =>
  Object.entries(
    users.reduce((countRecord: CountRecord, user) => {
      if (!user.onboardingData) {
        return countRecord;
      }

      user.onboardingData.forEach((x) => {
        if (!countRecord[x.question]) {
          countRecord[x.question] = {};
        }

        x.answer.forEach((y) => {
          if (!countRecord[x.question][y]) {
            countRecord[x.question][y] = 1;
          } else {
            countRecord[x.question][y] = countRecord[x.question][y] + 1;
          }
        });
      });

      return countRecord;
    }, {})
  ).map(([question, count]) => ({
    question,
    answers: Object.entries(count).map(([value, count]) => ({
      value,
      count,
    })),
  }));

const getActiveUsers = (record: Record<string, { year: number; month: number; count: number }[]>): ActiveUsersType[] => {
  return Object.entries(
    Object.values(record).reduce((countRecord: CountRecord, userActivities) => {
      userActivities.forEach((userActivity) => {
        if (!countRecord[userActivity.year]) {
          countRecord[userActivity.year] = {};
        }

        if (!countRecord[userActivity.year][userActivity.month]) {
          countRecord[userActivity.year][userActivity.month] = 1;
        } else {
          countRecord[userActivity.year][userActivity.month] = countRecord[userActivity.year][userActivity.month] + 1;
        }
      });

      return countRecord;
    }, {})
  ).map(([year, count]) => ({
    year: parseInt(year),
    months: Object.entries(count).map(([value, count]) => ({
      value: months[parseInt(value) - 1],
      count,
    })),
  }));
};
