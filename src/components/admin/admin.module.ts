import { Module } from '@nestjs/common';
import { ConfigModule } from '../../config/config.module';
import { UserRepository } from '../auth/repository/user.repository';
import { AdminResolver } from './admin.resolver';
import { AdminService } from './admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../auth/entity/user.entity';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([UserEntity])],
  providers: [AdminService, AdminResolver, UserRepository],
})
export class AdminModule {}
