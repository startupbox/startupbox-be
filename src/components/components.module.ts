import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { ProjectModule } from './project/project.module';
import { DiaryModule } from './diary/diary.module';
import { AdminModule } from './admin/admin.module';
import { ActivityModule } from './activity/activity.module';

@Module({
  imports: [AuthModule, ProjectModule, DiaryModule, AdminModule, ActivityModule],
})
export class ComponentsModule {}
