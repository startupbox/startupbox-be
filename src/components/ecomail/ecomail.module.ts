import { Module } from '@nestjs/common';
import { ConfigModule } from '../../config/config.module';
import { EcomailService } from './ecomail.service';

@Module({
  imports: [ConfigModule],
  providers: [EcomailService],
  exports: [EcomailService],
})
export class EcomailModule {}
