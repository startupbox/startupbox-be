import { Inject, Injectable } from '@nestjs/common';
import fetch from 'node-fetch';
import { ConfigService } from '../../config/config.service';
import { ConfigKeys } from '../../config/configKeys.enum';

@Injectable()
export class EcomailService {
  constructor(
    @Inject(ConfigService)
    private readonly config: ConfigService
  ) {}

  async addUserToMailingList(email: string): Promise<void> {
    await fetch(
      `${this.config.get(ConfigKeys.ECOMAIL_BASE_URL)}/lists/${this.config.get(ConfigKeys.ECOMAIL_SUBSCRIBERS_LIST_ID)}/subscribe`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          key: this.config.get(ConfigKeys.ECOMAIL_API_KEY),
        },
        body: JSON.stringify({
          subscriber_data: {
            email,
          },
          trigger_autoresponders: true,
        }),
      }
    );
  }
}
