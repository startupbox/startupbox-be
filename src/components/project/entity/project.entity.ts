import { BasicEntity } from '../../../utils/basicEntity';
import { Entity, Column, ManyToOne, JoinColumn, Index, OneToMany } from 'typeorm';
import { UserEntity } from '../../auth/entity/user.entity';
import { DiaryEntity } from '../../../components/diary/entity/diary.entity';

@Entity()
export class ProjectEntity extends BasicEntity {
  @Column()
  name: string;

  @Column('simple-array', { nullable: true })
  industry?: string[];

  @OneToMany(() => DiaryEntity, (diary: DiaryEntity) => diary.project)
  diary: DiaryEntity;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.projects, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;

  @Index()
  @Column()
  userId: string;

  @Column({ nullable: true })
  projectLogoUrl?: string;

  @Column({ default: false })
  archived: boolean;

  @Column({ nullable: true })
  scenarioId: string;
}
