import { ObjectType, Field } from '@nestjs/graphql';
import { BasicType } from '../../../utils/basicType';
import { UserType } from '../../auth/type/user.type';
import { ProjectPhase } from '../interfaces/projectPhase.enum';
import { DiaryType } from '../../diary/type/diary.type';

@ObjectType('Project')
export class ProjectType extends BasicType {
  @Field()
  name: string;

  @Field(() => [String], { nullable: true })
  industry?: string[];

  @Field(() => ProjectPhase, { nullable: true })
  phase?: ProjectPhase;

  @Field(() => UserType)
  user: UserType;

  @Field(() => [DiaryType])
  diary: DiaryType[];

  @Field({ nullable: true })
  projectLogoUrl?: string;

  @Field()
  archived: boolean;
}
