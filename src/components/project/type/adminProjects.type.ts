import { ObjectType, Field } from '@nestjs/graphql';
import { ProjectEntity } from '../entity/project.entity';
import { ProjectType } from './project.type';

@ObjectType('AdminProjects')
export class AdminProjectsType {
  @Field()
  count: number;

  @Field(() => [ProjectType])
  projects: ProjectEntity[];
}
