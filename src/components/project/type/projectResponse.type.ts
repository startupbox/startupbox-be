import { ObjectType, Field } from '@nestjs/graphql';
import { BasicResponseType } from '../../../utils/basicResponseType';
import { ProjectEntity } from '../entity/project.entity';
import { ProjectType } from './project.type';

@ObjectType('ProjectResponse')
export class ProjectResponseType extends BasicResponseType {
  @Field(() => ProjectType)
  project?: ProjectEntity;
}
