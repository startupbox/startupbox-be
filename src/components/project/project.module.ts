import { Module } from '@nestjs/common';
import { ConfigModule } from '../../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectService } from './project.service';
import { ProjectResolver } from './project.resolver';
import { ProjectRepository } from './repository/project.repository';
import { DiaryModule } from '../diary/diary.module';
import { CmsModule } from '../cms/cms.module';
import { ProjectEntity } from './entity/project.entity';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([ProjectEntity]), DiaryModule, CmsModule],
  providers: [ProjectService, ProjectResolver, ProjectRepository],
})
export class ProjectModule {}
