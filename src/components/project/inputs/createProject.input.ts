import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateProjectInput {
  @Field({ nullable: true })
  name?: string;

  @Field(() => [String], {
    nullable: true,
  })
  industry?: string[];

  @Field({ nullable: true })
  projectLogoUrl?: string;

  @Field({ nullable: true })
  projectPhase?: string;
}
