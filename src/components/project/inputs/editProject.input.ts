import { InputType, Field } from '@nestjs/graphql';
import { IsString } from 'class-validator';

@InputType()
export class EditProjectInput {
  @IsString()
  @Field()
  projectId: string;

  @Field({ nullable: true })
  name?: string;

  @Field(() => [String], { nullable: true })
  industry?: string[];

  @Field({ nullable: true })
  projectLogoUrl?: string;
}
