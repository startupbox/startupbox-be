import { Test, TestingModule } from '@nestjs/testing';
import { ProjectResolver } from './project.resolver';
import { ProjectService } from './project.service';

const mockProjectService = () => ({});

describe('ProjectResolver', () => {
  let resolver: ProjectResolver;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let service: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectResolver,
        {
          provide: ProjectService,
          useFactory: mockProjectService,
        },
      ],
    }).compile();

    resolver = module.get<ProjectResolver>(ProjectResolver);
    service = module.get<ProjectService>(ProjectService);
  });

  it('resolver should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });
});
