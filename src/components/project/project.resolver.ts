import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { UseGuards, ParseUUIDPipe } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/utils/gqlAuth.guard';
import { ProjectService } from './project.service';
import { ProjectType } from './type/project.type';
import { GetUser } from '../auth/utils/getUser.decorator';
import { UserEntity } from '../auth/entity/user.entity';
import { ProjectEntity } from './entity/project.entity';
import { CreateProjectInput } from './inputs/createProject.input';
import { RoleGuard } from '../auth/utils/roles.guard';
import { AllowedRoles } from '../auth/utils/allowedRoles.decorator';
import { UserRole } from '../auth/interfaces/userRole.enum';
import { EditProjectInput } from './inputs/editProject.input';
import { BasicResponseType } from '../../../src/utils/basicResponseType';
import { ProjectResponseType } from './type/projectResponse.type';
import { AdminProjectsType } from './type/adminProjects.type';

@Resolver('Project')
export class ProjectResolver {
  constructor(private readonly projectService: ProjectService) {}

  @Query(() => [ProjectType])
  @UseGuards(GqlAuthGuard)
  async getAllProjects(@GetUser() user: UserEntity): Promise<ProjectEntity[]> {
    return await this.projectService.getAllProjectsByUserId(user.id);
  }

  @Query(() => AdminProjectsType)
  @UseGuards(GqlAuthGuard, RoleGuard)
  @AllowedRoles(UserRole.ADMIN, UserRole.SUPER_ADMIN)
  async getAllProjectsAsAdmin(
    @GetUser() user: UserEntity,
    @Args('take') take: number = 30,
    @Args('skip') skip: number = 0
  ): Promise<AdminProjectsType> {
    if (user.role === UserRole.ADMIN) {
      return await this.projectService.getAllProjectsPerSubdomain(user.subdomain, take, skip);
    }
    return await this.projectService.getAllProjectsAsAdmin(take, skip);
  }

  @Query(() => ProjectType)
  @UseGuards(GqlAuthGuard)
  async getProjectById(@Args('projectId', ParseUUIDPipe) projectId: string): Promise<ProjectEntity> {
    return await this.projectService.getProjectById(projectId);
  }

  @Mutation(() => ProjectType)
  @UseGuards(GqlAuthGuard)
  async createProject(
    @GetUser() user: UserEntity,
    @Args('createProjectInput') createProjectInput: CreateProjectInput
  ): Promise<ProjectEntity> {
    const { name, industry, projectLogoUrl, projectPhase } = createProjectInput;
    return await this.projectService.createProject(user, name, industry, projectLogoUrl, projectPhase, user.subdomain);
  }

  @Mutation(() => ProjectResponseType)
  @UseGuards(GqlAuthGuard)
  async editProject(
    @GetUser() user: UserEntity,
    @Args('editProjectInput') editProjectInput: EditProjectInput
  ): Promise<ProjectResponseType | BasicResponseType> {
    try {
      const project = await this.projectService.editProject(editProjectInput, user.id);
      return {
        message: 'Project successfully edited.',
        code: '201',
        success: true,
        project,
      };
    } catch (e) {
      return {
        message: 'Something went wrong while editing project.',
        code: '500',
        success: false,
      };
    }
  }

  @Mutation(() => BasicResponseType)
  @UseGuards(GqlAuthGuard)
  async deleteProject(@GetUser() user: UserEntity, @Args('projectId') projectId: string): Promise<BasicResponseType> {
    try {
      await this.projectService.deleteProject(projectId, user.id);
      return {
        success: true,
        message: 'Project deleted successfully',
        code: '200',
      };
    } catch (e) {
      return {
        success: false,
        message: 'Something went wrong while deleting project.',
        code: '500',
      };
    }
  }
}
