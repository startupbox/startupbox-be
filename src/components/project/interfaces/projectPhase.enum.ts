import { registerEnumType } from '@nestjs/graphql';

export enum ProjectPhase {
  IDEALIZATION = 'IDEALIZATION',
  PROTOTYPING = 'PROTOTYPING',
  FINISHED_PRODUCT = 'FINISHED_PRODUCT',
  INVESTMENT_READY = 'INVESTMENT_READY',
}

registerEnumType(ProjectPhase, {
  name: 'ProjectPhase',
});
