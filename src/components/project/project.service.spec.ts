import { Test, TestingModule } from '@nestjs/testing';
import { ProjectService } from './project.service';
import { ProjectRepository } from './repository/project.repository';
import { DiaryService } from '../diary/diary.service';
import { CmsService } from '../cms/cms.service';

const mockProjectRepository = () => ({});
const mockDiaryService = () => ({});
const mockCmsService = () => ({});

describe('ProjectService', () => {
  let projectService: ProjectService;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let diaryService: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let repository: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let cmsService: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectService,
        {
          provide: DiaryService,
          useFactory: mockDiaryService,
        },
        {
          provide: ProjectRepository,
          useFactory: mockProjectRepository,
        },
        {
          provide: CmsService,
          useFactory: mockCmsService,
        },
      ],
    }).compile();

    projectService = module.get<ProjectService>(ProjectService);
    diaryService = module.get<DiaryService>(DiaryService);
    repository = module.get<ProjectRepository>(ProjectRepository);
    cmsService = module.get<CmsService>(CmsService);
  });

  it('projectService should be defined', () => {
    expect(projectService).toBeDefined();
  });

  it('diaryService should be defined', () => {
    expect(diaryService).toBeDefined();
  });

  it('repository should be defined', () => {
    expect(repository).toBeDefined();
  });

  it('cmsService should be defined', () => {
    expect(cmsService).toBeDefined();
  });
});
