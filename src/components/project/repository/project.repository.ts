import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { UserEntity } from '../../../components/auth/entity/user.entity';
import { ProjectEntity } from '../entity/project.entity';

@Injectable()
export class ProjectRepository extends Repository<ProjectEntity> {
  constructor(private dataSource: DataSource) {
    super(ProjectEntity, dataSource.createEntityManager());
  }

  async getProjectById(id: string): Promise<ProjectEntity | undefined> {
    const project = await this.createQueryBuilder('project')
      .where('project.id = :id', { id })
      .leftJoinAndSelect('project.user', 'user')
      .leftJoinAndSelect('project.diary', 'diary')
      .leftJoinAndSelect('diary.tasks', 'task')
      .getOne();
    return project;
  }

  async getProjectByUserId(userId: string): Promise<ProjectEntity[]> {
    const projects = await this.createQueryBuilder('project')
      .where('project.userId = :userId', { userId })
      .andWhere('project.archived = :archived', { archived: false })
      .innerJoinAndSelect('project.user', 'user')
      .innerJoinAndSelect('project.diary', 'diary')
      .leftJoinAndSelect('diary.tasks', 'task')
      .orderBy('diary.updatedAt', 'DESC')
      .getMany();
    return projects;
  }

  async getAllProjects(take: number, skip: number): Promise<ProjectEntity[]> {
    const projects = await this.createQueryBuilder('project')
      .where('project.archived = :archived', { archived: false })
      .innerJoinAndSelect('project.diary', 'diary')
      .leftJoinAndSelect('diary.tasks', 'task')
      .innerJoinAndSelect('project.user', 'user')
      .orderBy('diary.updatedAt', 'DESC')
      .take(take)
      .skip(skip)
      .getMany();
    return projects;
  }

  async getAllProjectsPerSubdomain(subdomain: string, take: number, skip: number): Promise<ProjectEntity[]> {
    const projects = await this.createQueryBuilder('project')
      .innerJoinAndSelect('project.diary', 'diary')
      .leftJoinAndSelect('diary.tasks', 'task')
      .innerJoinAndSelect('project.user', 'user')
      .where('user.subdomain = :subdomain', { subdomain })
      .orderBy('diary.updatedAt', 'DESC')
      .take(take)
      .skip(skip)
      .getMany();
    return projects;
  }

  async createProject(
    user: UserEntity,
    name: string,
    industry?: string[],
    projectLogoUrl?: string,
    scenarioId?: string
  ): Promise<ProjectEntity> {
    const p = new ProjectEntity();
    p.user = user;
    p.name = name;
    p.industry = industry;
    p.projectLogoUrl = projectLogoUrl;
    p.scenarioId = scenarioId;
    return this.save(p);
  }

  async editProject(project: ProjectEntity, name?: string, industry?: string[], projectLogoUrl?: string): Promise<ProjectEntity> {
    name && (project.name = name);
    industry && (project.industry = industry);
    projectLogoUrl && (project.projectLogoUrl = projectLogoUrl);
    return this.save(project);
  }

  async deleteProject(project: ProjectEntity): Promise<ProjectEntity> {
    project.archived = true;
    return this.save(project);
  }

  async subdomainProjectsCount(subdomain: string): Promise<number> {
    return await this.createQueryBuilder('project')
      .leftJoinAndSelect('project.user', 'user')
      .where('user.subdomain = :subdomain', { subdomain })
      .getCount();
  }
}
