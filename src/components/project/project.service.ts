import { Injectable, BadRequestException, InternalServerErrorException } from '@nestjs/common';
import { ProjectRepository } from './repository/project.repository';
import { UserEntity } from '../auth/entity/user.entity';
import { ProjectEntity } from './entity/project.entity';
import { DiaryService } from '../diary/diary.service';
import { DiaryStatus } from '../diary/interfaces/diaryStatus.enum';
import { EditProjectInput } from './inputs/editProject.input';
import { CmsService } from '../cms/cms.service';
import { AdminProjectsType } from './type/adminProjects.type';

@Injectable()
export class ProjectService {
  constructor(
    private readonly projectRepository: ProjectRepository,
    private readonly diaryService: DiaryService,
    private readonly cmsService: CmsService
  ) {}

  async getProjectById(projectId: string): Promise<ProjectEntity> {
    const project = await this.projectRepository.getProjectById(projectId);
    if (!project) {
      throw new BadRequestException(`Bad Request! Project ID ${projectId} does not exist!`);
    }
    return project;
  }

  async getAllProjectsByUserId(userId: string): Promise<ProjectEntity[]> {
    return await this.projectRepository.getProjectByUserId(userId);
  }

  async getAllProjectsAsAdmin(take: number, skip: number): Promise<AdminProjectsType> {
    const count = await this.projectRepository.count();
    const projects = await this.projectRepository.getAllProjects(take, skip);
    return {
      count,
      projects,
    };
  }

  async getAllProjectsPerSubdomain(subdomain: string, take: number, skip: number): Promise<AdminProjectsType> {
    const count = await this.projectRepository.subdomainProjectsCount(subdomain);
    const projects = await this.projectRepository.getAllProjectsPerSubdomain(subdomain, take, skip);
    return {
      count,
      projects,
    };
  }

  async createProject(
    user: UserEntity,
    name: string,
    industry?: string[],
    projectLogoUrl?: string,
    projectPhase?: string,
    subdomain?: string
  ): Promise<ProjectEntity> {
    try {
      const scenario = await this.cmsService.fetchScenarioByDomain(subdomain);
      const project = await this.projectRepository.createProject(user, name, industry, projectLogoUrl, scenario.id);
      await this.diaryService.createDiary(project, DiaryStatus.IN_PROGRESS, scenario, projectPhase);
      return project;
    } catch (e) {
      throw new BadRequestException(`Bad Request! Project already exists: ${e}`);
    }
  }

  async editProject(data: EditProjectInput, userId: string): Promise<ProjectEntity | null> {
    const project = await this.projectRepository.getProjectById(data.projectId);

    if (!project) {
      throw new BadRequestException(`Bad Request! Project does not exist.`);
    }

    try {
      if (data.projectLogoUrl) {
        await this.cmsService.deleteOldImageFromCms(project.projectLogoUrl);
      }
    } catch (e) {
      throw new InternalServerErrorException(e);
    }

    try {
      if (project.user.id !== userId) {
        throw new BadRequestException('Bad Request! Not an author of this project.');
      } else return await this.projectRepository.editProject(project, data.name, data.industry, data.projectLogoUrl);
    } catch (e) {
      throw new BadRequestException(`Bad Request! Could not edit project: ${e}`);
    }
  }

  async deleteProject(projectId: string, userId: string): Promise<ProjectEntity> {
    try {
      const project = await this.projectRepository.getProjectById(projectId);
      const diary = await this.diaryService.getDiaryByProjectId(projectId);
      if (!project || !diary) {
        return;
      }
      if (project.user.id !== userId) {
        throw new BadRequestException('Bad Request! Not an author of this project.');
      } else {
        project.projectLogoUrl && (await this.cmsService.deleteOldImageFromCms(project.projectLogoUrl));
        await this.diaryService.deleteDiary(diary);
        return await this.projectRepository.deleteProject(project);
      }
    } catch (e) {
      throw new BadRequestException(`Bad Request! Could not delete project: ${e}`);
    }
  }

  async archiveAllProjects(userId: string): Promise<ProjectEntity[]> {
    try {
      const projects = await this.getAllProjectsByUserId(userId);
      projects.forEach((project) => (project.archived = true));
      await Promise.all(
        projects.map(async (project) => {
          if (project.projectLogoUrl) {
            await this.cmsService.deleteOldImageFromCms(project.projectLogoUrl);
          }
        })
      );
      return projects;
    } catch (e) {
      throw new BadRequestException('Something went wrong');
    }
  }
}
