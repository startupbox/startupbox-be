import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { ConfigKeys } from '../../config/configKeys.enum';
import { DiaryScenario } from '../diary/interfaces/scenario.interface';
import axios from 'axios';
import { CmsTask } from '../diary/interfaces/cmsTask.interface';
import { GraphcmsStage } from '../diary/interfaces/stage.enum';

@Injectable()
export class CmsService {
  constructor(
    @Inject(ConfigService)
    private readonly configService: ConfigService
  ) {}

  async graphcmsCall(query: string): Promise<any> {
    const result = (
      await axios.post(
        this.configService.get(ConfigKeys.GRAPHCMS_API_URL),
        {
          query,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.configService.get(ConfigKeys.GRAPHCMS_TOKEN)}`,
          },
        }
      )
    ).data.data;

    return result;
  }

  async fetchScenarioById(scenarioId: string, language?: string): Promise<DiaryScenario> {
    // default scenario id for existing projects without the ID specified.
    const id = scenarioId ? scenarioId : 'ckv8dkbo8qyhp0b56op8cr6i8';
    const locale = language ? language : 'cz';

    const query = `
      query {
        diaryScenario(where: {id: "${id}"}, stage: ${this.configService.defaultCmsStage()}) {
          id
          name
          phases(locales:[${locale}]) {
            label
            title
            components(locales:[${locale}]) {
              title
              label
              tasks(locales:[${locale}]) {
                title
                id
                howToPage
                subtitle
                question
                duration
                thumbnail {
                  id
                  height
                  width
                  url
                }
              }
            }
          }
        }
      }`;

    const scenario = await this.graphcmsCall(query);
    return scenario.diaryScenario;
  }

  async fetchScenarioByDomain(subdomain?: string, language?: string): Promise<DiaryScenario> {
    const checkIfDiaryExists = async (subdomain) => {
      const query = `
        query {
          diaryScenarios(where: { client_contains_some: ["${subdomain}"]}, stage: ${this.configService.defaultCmsStage()}) {
            id
          }
        }
      `;

      const scenario = await this.graphcmsCall(query);

      return scenario.diaryScenarios;
    };

    const diaries = await checkIfDiaryExists(subdomain);

    const diary = diaries.length ? subdomain : 'default';
    const locale = language ? language : 'cz';

    const query = `
      query {
        diaryScenarios(where: { client_contains_some: ["${diary}"]}, stage: ${this.configService.defaultCmsStage()}) {
          id
          name
          phases(locales:[${locale}]) {
            label
            title
            components(locales:[${locale}]) {
              label
              title
              tasks(locales:[${locale}]) {
                title
                id
                howToPage
                subtitle
                question
                duration
                thumbnail {
                  id
                  height
                  width
                  url
                }
              }
            }
          }
        }
      }`;

    const scenario = await this.graphcmsCall(query);
    return scenario.diaryScenarios[0];
  }

  async findTaskFromCms(taskId: string, language?: string): Promise<CmsTask> {
    const locale = language ? language : 'cz';
    const query = `
      query {
        task(where: {id: "${taskId}"}, stage: ${this.configService.defaultCmsStage()}, locales:[${locale}]) {
          howToPage
          title
          subtitle
          question
          duration
          thumbnail {
            id
            height
            width
            url
          }
        }
      }
    `;

    const data = await this.graphcmsCall(query);
    return data.task;
  }

  async fetchRecommendedTasks(segmentation: string): Promise<string[]> {
    const recommendedTasksIds = [];

    const getRecommendedTasks = (tasks) => {
      tasks[0].recommendedTasks.forEach((task) => {
        recommendedTasksIds.push(task.id);
      });
    };

    const getTasksFromCms = async (name: string) => {
      const query = `
      query {
        recommendedTasks(where: {name:"${name}"}, stage: ${this.configService.defaultCmsStage()}) {
          recommendedTasks {
            id
          }
        }
      }
    `;

      const data = await this.graphcmsCall(query);
      return data.recommendedTasks;
    };

    const tasks = await getTasksFromCms(segmentation);

    if (!tasks.length) {
      const defaultTasks = await getTasksFromCms('Default');
      getRecommendedTasks(defaultTasks);
    } else getRecommendedTasks(tasks);

    return recommendedTasksIds;
  }

  async fetchImage(imageHandle: string): Promise<{ id: string }> {
    const query = `
    query {
      assets(where: {handle:"${imageHandle}"}, stage: ${GraphcmsStage.DRAFT}) {
        id
      }
    }
  `;

    const image = await this.graphcmsCall(query);

    return image.assets[0];
  }

  async deleteOldImageFromCms(imageUrl: string): Promise<void> {
    if (!imageUrl) return;
    const imageHandle = imageUrl.split('/').pop();
    const image = await this.fetchImage(imageHandle);

    if (!image) return;

    const mutation = `
    mutation {
      deleteAsset(where: { id:"${image.id}" }) {
        id
      }
    }
  `;

    await axios.post(
      this.configService.get(ConfigKeys.GRAPHCMS_API_URL),
      {
        query: mutation,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.configService.get(ConfigKeys.GRAPHCMS_TOKEN)}`,
          'gcms-stage': GraphcmsStage.DRAFT,
        },
      }
    );
  }
}
