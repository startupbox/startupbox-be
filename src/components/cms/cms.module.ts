import { Module } from '@nestjs/common';
import { ConfigModule } from '../../config/config.module';
import { CmsService } from './cms.service';

@Module({
  imports: [ConfigModule],
  providers: [CmsService],
  exports: [CmsService],
})
export class CmsModule {}
