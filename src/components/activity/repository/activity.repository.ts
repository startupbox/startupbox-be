import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { UserEntity } from '../../auth/entity/user.entity';
import { ActivityEntity } from '../entity/activity.entity';

@Injectable()
export class ActivityRepository extends Repository<ActivityEntity> {
  constructor(private dataSource: DataSource) {
    super(ActivityEntity, dataSource.createEntityManager());
  }

  async createActivity(user: UserEntity, name: string): Promise<ActivityEntity> {
    const p = new ActivityEntity();
    p.user = user;
    p.name = name;
    return this.save(p);
  }
}
