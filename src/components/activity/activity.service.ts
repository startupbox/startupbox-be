import { Injectable } from '@nestjs/common';
import { UserEntity } from '../auth/entity/user.entity';
import { ActivityRepository } from './repository/activity.repository';

@Injectable()
export class ActivityService {
  constructor(private readonly activityRepository: ActivityRepository) {}

  async createActivity(user: UserEntity, name: string): Promise<void> {
    await this.activityRepository.createActivity(user, name);
  }
}
