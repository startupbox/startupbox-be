import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { BasicEntity } from '../../../utils/basicEntity';
import { UserEntity } from '../../auth/entity/user.entity';

@Entity()
export class ActivityEntity extends BasicEntity {
  @Column()
  name: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.projects, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;

  @Index()
  @Column()
  userId: string;
}
