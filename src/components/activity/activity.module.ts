import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { ActivityService } from './activity.service';
import { ActivityEntity } from './entity/activity.entity';
import { ActivityRepository } from './repository/activity.repository';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([ActivityEntity])],
  providers: [ActivityService, ActivityRepository],
})
export class ActivityModule {}
